docker build --force-rm  -f images/orderer/Dockerfile \
        --build-arg GO_VER=1.20.3 \
        --build-arg UBUNTU_VER=20.04 \
        --build-arg FABRIC_VER=2.5.1 \
        --build-arg TARGETARCH=amd64 \
        --build-arg TARGETOS=linux \
        --build-arg GO_TAGS= \
        -t hyperledger/fabric-orderer ./
