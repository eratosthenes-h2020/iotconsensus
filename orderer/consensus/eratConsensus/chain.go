package eratConsensus

import (
	"context"
	"crypto/ecdsa"
	"crypto/elliptic"
	"crypto/x509"
	"encoding/hex"
	"encoding/pem"
	"fmt"
	"os"
	"sync"
	"sync/atomic"
	"time"

	"code.cloudfoundry.org/clock"
	"github.com/golang/protobuf/proto"
	"github.com/hyperledger/fabric-protos-go/common"
	cb "github.com/hyperledger/fabric-protos-go/common"
	"github.com/hyperledger/fabric-protos-go/orderer"
	"github.com/hyperledger/fabric-protos-go/orderer/etcdraft"
	"github.com/hyperledger/fabric/bccsp"
	"github.com/hyperledger/fabric/common/flogging"
	"github.com/hyperledger/fabric/orderer/common/cluster"
	"github.com/hyperledger/fabric/orderer/common/types"
	"github.com/hyperledger/fabric/orderer/consensus"
	"github.com/hyperledger/fabric/protoutil"
	"github.com/pkg/errors"
	"github.com/vechain/go-ecvrf"
	"go.etcd.io/etcd/raft/v3/raftpb"
	pb "go.etcd.io/etcd/raft/v3/raftpb"
)

// TODO majority needs to be calculated as well
// majority of reputable nodes hardcoded
var majorityOfReputables int = 2

type submit struct {
	req    *orderer.SubmitRequest
	leader chan uint64
}

type vrfSenderRequest struct {
	vrf              []byte
	sender           uint64
	request          *orderer.SubmitRequest
	blockDataEntries []raftpb.Entry
	blockIndex       uint64 // added this lately
}

// Configurator is used to configure the communication layer
// when the chain starts.
type Configurator interface {
	Configure(channel string, newNodes []cluster.RemoteNode)
}

// TODO i probably do not need this interface
// TODO i am actually using cluster.RPC struct
// RPC is used to mock the transport layer in tests.
type RPC interface {
	SendConsensus(dest uint64, msg *orderer.ConsensusRequest) error
	SendSubmit(dest uint64, request *orderer.SubmitRequest, report func(err error)) error
}

// BlockPuller is used to pull blocks from other OSN
type BlockPuller interface {
	PullBlock(seq uint64) *common.Block
	HeightsByEndpoints() (map[string]uint64, error)
	Close()
}

type apply struct {
	entries []raftpb.Entry
}

type blockAndVotes struct {
	entries []raftpb.Entry
	votes   int
}

type blockAndVRF struct {
	//vrfIndex   float32
	vrfIndex   []byte // the calculated vrf
	entries    []raftpb.Entry
	blockIndex uint64
	//sender     uint64
}

type quorumVotesResult struct {
	majorityFound bool
	vrfIndex      float32
}

type blockProposalStruct struct {
	blockData *common.Block
	nodeVRF   []byte
	//nodeVRF   float32
}

// CreateBlockPuller is a function to create BlockPuller on demand.
// It is passed into chain initializer so that tests could mock this.
type CreateBlockPuller func() (BlockPuller, error)

// Chain implements the consensus.Chain interface
type Chain struct {
	configurator Configurator

	// TODO update i need to use RPC
	rpc RPC

	raftID          uint64
	channelID       string
	lastKnownLeader uint64
	ActiveNodes     atomic.Value

	// reputation
	// maps nodeID to a reputation score
	reputationNodes map[uint64]float32

	// map vfr --> struct containing block along with its number of votes
	votingBlocks map[uint64]map[string]blockAndVotes

	// map of vrf pairs pending
	//pendingVrfPairs     []vrfSenderRequest
	//pendingVrfPairs     map[*common.Envelope]vrfSenderRequest
	pendingVrfPairs     map[string]vrfSenderRequest // associates payload to vrfSenderRequest
	pendingVrfPairsLock sync.RWMutex

	processingABlock     bool
	counterOfPeerSubmits int

	batches [][]*common.Envelope

	submitC                       chan *submit
	applyC                        chan apply
	vrfC                          chan vrfSenderRequest
	bestVRF                       vrfSenderRequest
	ownBlockDataC                 chan uint64
	blockDataEntriesConfirmationC chan struct{}
	//bestVRF map[uint64]vrfSenderRequest // maps block index with a vrfSenderRequest struct

	votedVRF []byte

	ownVRF              []byte
	ownBlockDataEntries []pb.Entry

	// currentlyProcessingBlock

	/*
		currentlyAvailableSpotLock sync.RWMutex
		currentlyProcessingPayload []byte
	*/
	calculatedVRF bool
	//nodeVRF           float32
	nodeVRF           []byte
	calculatedVRFLock sync.RWMutex

	haltC                 chan struct{} // Signals to goroutines that the chain is halting
	doneC                 chan struct{} // Closes when the chain halts
	startC                chan struct{} // Closes when the node is started
	errorC                chan struct{} // returned by Errored()
	blockSentToEveryoneC  chan struct{} // Block is proposed to every other orderer of the network
	blockVotesToEveryoneC chan struct{} // Vote for a specific block is sent to everyone if and only if the node is reputable

	votingPhaseStartC      chan struct{}
	blockProposalPhaseC    chan struct{}
	quorum4IndexC          chan blockAndVRF
	quorumProposals4IndexC chan uint64
	blockProposalC         chan blockProposalStruct

	//blockProposalC chan blockProposalStruct
	blockProposals map[uint64]int

	quorumBlockProposals map[uint64]bool
	txnsMinted           map[string]struct{} //latest addition

	// added this
	// maps a sender with each publickey
	orderersPublicKeys map[uint64]*ecdsa.PublicKey

	//txnsInFlight map[*common.Envelope]*orderer.SubmitRequest
	txnsInFlight map[string]*orderer.SubmitRequest

	raftMetadataLock sync.RWMutex
	//quorumVotesLock sync.
	quorumVotesLock sync.Mutex

	blockInflight int // number of in flight blocks

	clock clock.Clock // tests can inject a fake clock

	batchSize uint32

	support consensus.ConsenterSupport

	lastBlock    *common.Block
	appliedIndex uint64

	fresh bool // indicate if this is a fresh node or not

	leader uint64 // indicates if it is either a leader or not

	// this is exported so that test can use `Node.Status()` to get raft node status.
	// TODO add node later
	Node *node
	opts Options

	//Metrics *Metrics
	logger *flogging.FabricLogger

	createPuller CreateBlockPuller // func used to create BlockPuller on demand

	counter int

	haltCallback func()

	// timer initializations
	votingPhaseTicking    bool
	votingPhaseTimer      clock.Timer
	startVotingPhaseTimer func()
	//quorumVotes           bool
	quorumVotes map[uint64]quorumVotesResult

	reachedQuorum4Index map[uint64]blockAndVRF

	statusReportMutex sync.Mutex
	consensusRelation types.ConsensusRelation
	status            types.Status

	// BCCSP instance
	CryptoProvider bccsp.BCCSP

	//leadershipTransferInProgress uint32
}

// Halt stops the chain.
func (c *Chain) Halt() {
	c.stop()
}

func (c *Chain) stop() bool {
	select {
	case <-c.startC:
	default:
		c.logger.Warn("Attempted to halt a chain that has not started")
		return false
	}

	select {
	case c.haltC <- struct{}{}:
	case <-c.doneC:
		return false
	}
	<-c.doneC

	c.statusReportMutex.Lock()
	defer c.statusReportMutex.Unlock()
	c.status = types.StatusInactive

	return true
}

// halt stops the chain and calls the haltCallback function, which allows the
// chain to transfer responsibility to a follower or the inactive chain registry when a chain
// discovers it is no longer a member of a channel.
func (c *Chain) halt() {
	if stopped := c.stop(); !stopped {
		c.logger.Info("This node was stopped, the haltCallback will not be called")
		return
	}
	if c.haltCallback != nil {
		c.haltCallback() // Must be invoked WITHOUT any internal lock

		c.statusReportMutex.Lock()
		defer c.statusReportMutex.Unlock()

		// If the haltCallback registers the chain in to the inactive chain registry (i.e., system channel exists) then
		// this is the correct consensusRelation. If the haltCallback transfers responsibility to a follower.Chain, then
		// this chain is about to be GC anyway. The new follower.Chain replacing this one will report the correct
		// StatusReport.
		c.consensusRelation = types.ConsensusRelationConfigTracker
	}

	// active nodes metric shouldn't be frozen once a channel is stopped.
	//c.Metrics.ActiveNodes.Set(float64(0))
}

// StatusReport returns the ConsensusRelation & Status
func (c *Chain) StatusReport() (types.ConsensusRelation, types.Status) {
	c.statusReportMutex.Lock()
	defer c.statusReportMutex.Unlock()

	return c.consensusRelation, c.status
}

func (c *Chain) fillReputationNodesMap() {

	//create the reputationNodesMap
	c.reputationNodes[1] = 0.6
	c.reputationNodes[2] = 0.7
	c.reputationNodes[3] = 0.3
	c.reputationNodes[4] = 0.4
	c.reputationNodes[5] = 0.3
	c.reputationNodes[6] = 0.3
	c.reputationNodes[7] = 0.3
	c.reputationNodes[8] = 0.3
	c.reputationNodes[9] = 0.3
	c.reputationNodes[10] = 0.3
	c.reputationNodes[11] = 0.3

	c.logger.Info("ReputationNodes --> ", c.reputationNodes)
}

// Options contains all the configurations relevant to the chain.
type Options struct {
	RPCTimeout time.Duration
	RaftID     uint64

	Clock clock.Clock

	//WALDir               string
	//SnapDir              string
	//SnapshotIntervalSize uint32

	// This is configurable mainly for testing purpose. Users are not
	// expected to alter this. Instead, DefaultSnapshotCatchUpEntries is used.
	//SnapshotCatchUpEntries uint64

	//MemoryStorage MemoryStorage
	Logger *flogging.FabricLogger

	//TickInterval      time.Duration
	//ElectionTick      int
	//HeartbeatTick     int
	MaxSizePerMsg     uint64
	MaxInflightBlocks int

	// BlockMetadata and Consenters should only be modified while under lock
	// of raftMetadataLock
	BlockMetadata *etcdraft.BlockMetadata
	Consenters    map[uint64]*etcdraft.Consenter

	// MigrationInit is set when the node starts right after consensus-type migration
	MigrationInit bool

	//Metrics *Metrics
	Cert []byte

	// counter for the first invocation of a chaincode Order
	counter int

	//EvictionSuspicion   time.Duration
	//LeaderCheckInterval time.Duration
}

// NewChain constructs a chain object.
// removed observeC
func NewChain(
	support consensus.ConsenterSupport,
	opts Options,
	conf Configurator,
	rpc RPC,
	cryptoProvider bccsp.BCCSP,
	f CreateBlockPuller,
	haltCallback func(),
) (*Chain, error) {

	lg := opts.Logger.With("channel", support.ChannelID(), "node", opts.RaftID)

	b := support.Block(support.Height() - 1)
	lg.Info("Current Height: ", support.Height())

	if b == nil {
		return nil, errors.Errorf("failed to get last block")
	}

	c := &Chain{
		configurator:                  conf,
		rpc:                           rpc,
		channelID:                     support.ChannelID(),
		raftID:                        opts.RaftID,
		submitC:                       make(chan *submit),
		vrfC:                          make(chan vrfSenderRequest),
		applyC:                        make(chan apply),
		haltC:                         make(chan struct{}),
		doneC:                         make(chan struct{}),
		startC:                        make(chan struct{}),
		errorC:                        make(chan struct{}),
		blockSentToEveryoneC:          make(chan struct{}),
		blockVotesToEveryoneC:         make(chan struct{}),
		votingPhaseStartC:             make(chan struct{}),
		blockProposalPhaseC:           make(chan struct{}),
		quorum4IndexC:                 make(chan blockAndVRF),
		quorumProposals4IndexC:        make(chan uint64),
		ownBlockDataC:                 make(chan uint64),
		blockDataEntriesConfirmationC: make(chan struct{}),
		txnsMinted:                    make(map[string]struct{}),

		//bestVRF:                make(map[uint64]vrfSenderRequest),

		// initialize reputationNodes map
		reputationNodes: make(map[uint64]float32),
		// initialize votingBlocks map
		// associates a blockIndex with a map associationg vrf with blockAndVotes struct
		votingBlocks: make(map[uint64]map[string]blockAndVotes),

		//pendingVrfPairs: make(map[*common.Envelope]vrfSenderRequest),
		pendingVrfPairs: make(map[string]vrfSenderRequest),

		//	txnsInFlight: make(map[*common.Envelope]*orderer.SubmitRequest),
		txnsInFlight: make(map[string]*orderer.SubmitRequest),

		// associates a block Index with a boolean indicating if majority votes received
		quorumVotes: make(map[uint64]quorumVotesResult),

		reachedQuorum4Index: make(map[uint64]blockAndVRF),

		// associates blockProposals received for a specified blockIndex
		blockProposals: make(map[uint64]int),

		//
		quorumBlockProposals: make(map[uint64]bool),

		// initialization
		orderersPublicKeys: make(map[uint64]*ecdsa.PublicKey),

		// we will use this instead
		blockProposalC: make(chan blockProposalStruct, opts.MaxInflightBlocks),
		support:        support,
		appliedIndex:   opts.BlockMetadata.RaftIndex,
		lastBlock:      b,
		//sizeLimit:         sizeLimit,
		//lastSnapBlockNum:  snapBlkNum,
		//confState:         cc,
		createPuller: f,
		clock:        opts.Clock,
		//haltCallback:      haltCallback,
		consensusRelation: types.ConsensusRelationConsenter,
		status:            types.StatusActive,
		logger:            lg,
		opts:              opts,

		counter: 0,

		CryptoProvider: cryptoProvider,
	}

	// initialize voting timer and functions
	c.votingPhaseTimer = c.clock.NewTimer(time.Second)

	c.startVotingPhaseTimer = func() {
		if !c.votingPhaseTicking {
			c.logger.Info("[VOTINGPHASE] --> votingPhase timer starting")
			//votingPhaseTimer.Reset(2 * (c.support.SharedConfig().BatchTimeout()))
			c.votingPhaseTimer.Reset(2 * time.Second)
		}
	}

	disseminator := &Disseminator{RPC: c.rpc}
	disseminator.UpdateMetadata(nil) // initialize
	c.ActiveNodes.Store([]uint64{})

	// added now
	c.nodeVRF, _ = hex.DecodeString("00")

	c.Node = &node{
		chainID:  c.channelID,
		chain:    c,
		logger:   c.logger,
		nodeID:   c.opts.RaftID,
		rpc:      disseminator,
		metadata: c.opts.BlockMetadata,
		state:    PreConsesusState,
	}

	c.logger.Info("Chain has been created without any errors")
	return c, nil

}

func pemToDER(pemBytes []byte, id uint64, certType string, logger *flogging.FabricLogger) ([]byte, error) {
	bl, _ := pem.Decode(pemBytes)
	if bl == nil {
		logger.Errorf("Rejecting PEM block of %s TLS cert for node %d, offending PEM is: %s", certType, id, string(pemBytes))
		return nil, errors.Errorf("invalid PEM block")
	}
	return bl.Bytes, nil
}

// Configure submits config type transactions for ordering.
func (c *Chain) Configure(env *common.Envelope, configSeq uint64) error {
	// TODO uncomment the return which uses the Submit
	//c.Metrics.ConfigProposalsReceived.Add(1)
	//return c.Submit(&orderer.SubmitRequest{LastValidationSeq: configSeq, Payload: env, Channel: c.channelID}, 0)
	return nil
}

// Start instructs the orderer to begin serving the chain and keep it current.
func (c *Chain) Start() {

	maxMessageCount := c.support.SharedConfig().BatchSize()
	batchSize := maxMessageCount.MaxMessageCount

	c.logger.Infof("Starting a new erat Chain with batch size = %v", batchSize)

	c.batchSize = batchSize

	// TODO hardcoded for now
	// create the reputable nodes map for each orderer
	c.fillReputationNodesMap()

	if err := c.configureComm(); err != nil {
		c.logger.Errorf("Failed to start chain, aborting: +%v", err)
		close(c.doneC)
		return
	}

	// isJoin bool snippet this will never be true in our case
	isJoin := c.support.Height() > 1
	if isJoin && c.opts.MigrationInit {
		isJoin = false
		c.logger.Infof("Consensus-type migration detected, starting new raft node on an existing channel; height=%d", c.support.Height())
	}

	c.Node.start(c.fresh, isJoin)

	close(c.startC)
	close(c.errorC)

	// TODO gc --> garbage collection removed
	//go c.gc()
	go c.run()

	// TODO check which of those need to be commented out
	//es := c.newEvictionSuspector()

	/*
		interval := DefaultLeaderlessCheckInterval
		if c.opts.LeaderCheckInterval != 0 {
			interval = c.opts.LeaderCheckInterval
		}

		c.periodicChecker = &PeriodicCheck{
			Logger:        c.logger,
			Report:        es.confirmSuspicion,
			ReportCleared: es.clearSuspicion,
			CheckInterval: interval,
			Condition:     c.suspectEviction,
		}
		c.periodicChecker.Run()
	*/
}

func (c *Chain) DeleteStructsAndReset() {

	nodeStateString := consensusStateToString(c.Node.state)
	//blockNumberToBeAdded := c.lastBlock.Header.Number + 1
	var blockNumberToBeAdded uint64
	if c.Node.state != VotingPhase {
		blockNumberToBeAdded = c.lastBlock.Header.Number
	} else {
		blockNumberToBeAdded = c.lastBlock.Header.Number + 1
	}

	c.raftMetadataLock.Lock()
	delete(c.votingBlocks, blockNumberToBeAdded)
	delete(c.blockProposals, blockNumberToBeAdded)
	c.logger.Infof("[%s] --> Deleting c.bestVRF = %v for block num = %v", nodeStateString, blockNumberToBeAdded)
	c.bestVRF = vrfSenderRequest{}
	//delete(c.bestVRF, blockNumberToBeAdded)
	c.raftMetadataLock.Unlock()
	c.logger.Infof("[%s] --> Deleting c.reachedQuorum4Index for block num = %v", nodeStateString, blockNumberToBeAdded)
	delete(c.reachedQuorum4Index, blockNumberToBeAdded)

	var err error
	c.nodeVRF, err = hex.DecodeString("00")
	if err != nil {
		c.logger.Panicf("[%s] Error in decoding zeroHex string: %v", nodeStateString, err)
	}
	c.ownVRF, _ = hex.DecodeString("00")
	c.quorumVotesLock.Lock()
	delete(c.quorumVotes, blockNumberToBeAdded)
	c.quorumVotesLock.Unlock()
	c.logger.Infof("[%s] Setting c.calculatedVRF back to false....", nodeStateString)
	c.calculatedVRFLock.Lock()
	c.calculatedVRF = false
	c.calculatedVRFLock.Unlock()

	// TODO added it
	//c.txnsInFlight = nil

	c.logger.Infof("[%s] Resetting the c.txnsInFlight map")
	for index := range c.txnsInFlight {
		delete(c.txnsInFlight, index)
	}

	// TODO
	// c.ownBlockDataEntries = nil

	c.Node.state = BlockProposal

	c.processingABlock = false
	c.logger.Infof("Finished DeleteStructsAndReset")

}

// func (c *Chain) reconstructTxnsFromBlockEntries(blockEntries []byte) []*common.Envelope {
func (c *Chain) reconstructTxnsFromBlockEntries(blockEntries []byte) map[string]*common.Envelope {
	var block *common.Block
	var err error

	blockTxnsMap := make(map[string]*common.Envelope)

	c.logger.Infof("Before Unmarshal blockEntries in reconstructTxnsFromBlockEntries")
	//if err := proto.Unmarshal(blockEntries, block); err != nil {
	if block, err = protoutil.UnmarshalBlock(blockEntries); err != nil {
		c.logger.Panicf("error in unmarshalling entries block to block --> %v", err)
	}
	// retrieve envelopes from myBlock.Data
	//var blkTxns []*common.Envelope
	c.logger.Infof("Before for loop in reconstructTxnsFromBlockEntries")
	for _, envBytes := range block.Data.Data {
		var txn *common.Envelope
		var err error
		if txn, err = protoutil.UnmarshalEnvelope(envBytes); err != nil {
			c.logger.Panicf("error in unmarshalling block data to a txn --> %v", err)
		}
		//blkTxns = append(blkTxns, txn)
		//blkTxns = append(blkTxns, txn)
		blockTxnsMap[string(txn.Payload)] = txn
	}

	//return blkTxns
	return blockTxnsMap

}

func (c *Chain) removePotentialDupTxns() {
	//blockIndexAdded := c.lastBlock.Header.Number

	c.logger.Infof("Before accessing myownBlkEntries")
	myblkEntries := c.ownBlockDataEntries[0].Data
	c.logger.Infof("After accessing myownBlkEntries")
	ownTxns := c.reconstructTxnsFromBlockEntries(myblkEntries)

	c.logger.Infof("Before accessing proposedBlkEntries")
	proposedBlkEntries := c.bestVRF.blockDataEntries[0].Data
	//proposedBlkEntries := c.bestVRF[blockIndexAdded].blockDataEntries[0].Data
	c.logger.Infof("After accessing proposedBlkEntries")
	proposedTxns := c.reconstructTxnsFromBlockEntries(proposedBlkEntries)

	// create a hashmap of proposedTxns
	//proposedTxnsMap := make(map[*common.Envelope]bool)

	//	proposedTxnsMap := make(map[string]bool) // associates string(Envelope.Payload) with boll

	var txnsToBeProposedAgain []*common.Envelope

	// removed now
	//for _, txn := range proposedTxns {
	//	proposedTxnsMap[string(txn.Payload)] = true
	//}

	// create a slice of txns that will be proposed again
	for _, txn := range ownTxns {
		txnPayload := string(txn.Payload)
		//if !proposedTxnsMap[txnPayload] {
		//	txnsToBeProposedAgain = append(txnsToBeProposedAgain, txn)
		//}

		if _, exists := proposedTxns[txnPayload]; exists {
			delete(proposedTxns, txnPayload)
		} else {
			txnsToBeProposedAgain = append(txnsToBeProposedAgain, txn)
		}
	}

	c.pendingVrfPairsLock.Lock()
	// Check if there are any txns in the pendingTxns left
	for _, txn := range proposedTxns {
		txnPayload := string(txn.Payload)
		if _, exists := c.pendingVrfPairs[txnPayload]; exists {
			delete(c.pendingVrfPairs, txnPayload)
			delete(proposedTxns, txnPayload)
		} else {
			c.txnsMinted[txnPayload] = struct{}{}
		}
	}

	// added now be careful!!!!!!!!!!!!!!
	/*
		for _, txn := range ownTxns {
			if _, exists := c.pendingVrfPairs[string(txn.Payload)]; exists {
				delete(c.pendingVrfPairs, string(txn.Payload))
			}
		}
	*/

	for _, txn := range txnsToBeProposedAgain {
		nodeVRF, _ := hex.DecodeString("00")
		request4Txn := c.txnsInFlight[string(txn.Payload)]
		vrfpair := vrfSenderRequest{
			vrf:     nodeVRF,
			sender:  0,
			request: request4Txn,
		}

		c.pendingVrfPairs[string(txn.Payload)] = vrfpair

	}
	c.pendingVrfPairsLock.Unlock()

	//return txnsToBeProposedAgain

}

// func (c *Chain) CheckIfPendingTxns(txnsToProposeAgain []*common.Envelope) {
func (c *Chain) CheckIfPendingTxns() {

	nodeStateString := consensusStateToString(c.Node.state)
	//var txnsExtracted = 0
	//if len(txnsToProposeAgain) != 0 {
	//	txnsExtracted = len(txnsToProposeAgain)

	//}

	blockNumberToBeAdded := c.lastBlock.Header.Number

	c.pendingVrfPairsLock.Lock()
	if len(c.pendingVrfPairs) != 0 {
		c.logger.Infof("[%s] --> CheckIfPending There are pending txns length = %v, so they need to be included in the current block = %v", nodeStateString, len(c.pendingVrfPairs), blockNumberToBeAdded)

		// critical section

		//go func() {
		//	c.pendingVrfPairsLock.Lock()
		var txnsExtracted = 0
		for txn, pendingPair := range c.pendingVrfPairs {
			//requestPending := pendingPair.request
			var reqPending orderer.SubmitRequest
			//reqPending = *pendingPair.request
			//reqPending = pendingPair.request
			senderOfPendingRequest := pendingPair.sender

			reqPending.Channel = pendingPair.request.Channel
			reqPending.LastValidationSeq = pendingPair.request.LastValidationSeq
			reqPending.Payload = pendingPair.request.Payload
			reqPending.Vrf = pendingPair.request.Vrf

			// invoking c.Submit for the pending txn
			c.logger.Infof("[%s] I am about to invoke a pending pair", nodeStateString)
			//go c.Submit(requestPending, senderOfPendingRequest)
			if _, exists := c.txnsMinted[txn]; !exists {
				delete(c.pendingVrfPairs, txn)
				go c.Submit(&reqPending, senderOfPendingRequest)
				txnsExtracted++
				if uint32(txnsExtracted) == c.batchSize {
					c.logger.Infof("[%s] I extracted %v txns from the pendingPair queue", nodeStateString, c.batchSize)
					break
				}
			} else {
				delete(c.pendingVrfPairs, txn)
				delete(c.txnsMinted, txn)

			}

		}
		//c.pendingVrfPairs = c.pendingVrfPairs[txnsExtracted:]
		//c.pendingVrfPairsLock.Unlock()
	} else {
		c.logger.Infof("[%s] There are not any pending txns...", nodeStateString)
	}
	c.pendingVrfPairsLock.Unlock()

}

func (c *Chain) run() {

	// initialize the timer that will be used in timerC
	// used in propose if batches length = 0
	ticking := false
	//blockProposalPhaseTicking := false
	//votingPhaseTicking := false

	// timer of the original code that did the block creation
	timer := c.clock.NewTimer(time.Second)
	// we need a stopped timer rather than nil,
	// because we will be select waiting on timer.C()
	if !timer.Stop() {
		<-timer.C()
	}

	// timer for creating a block in the BlockProposal phase
	blockProposalTimer := c.clock.NewTimer(time.Second)
	if !blockProposalTimer.Stop() {
		<-blockProposalTimer.C()
	}

	blockProposalPhaseTimer := c.clock.NewTimer(time.Second)
	if !blockProposalPhaseTimer.Stop() {
		<-blockProposalPhaseTimer.C()
	}

	if !c.votingPhaseTimer.Stop() {
		<-c.votingPhaseTimer.C()
	}

	// added this timer to understand when the phase is over

	startBlockProposalTimer := func() {
		if !ticking {
			ticking = true
			c.logger.Infof("[%s] --> startBlockProposalTimer starting", consensusStateToString(c.Node.state))
			//blockProposalTimer.Reset(c.support.SharedConfig().BatchTimeout())
			// Our block duration is 1 second istead of 2
			blockProposalTimer.Reset(1 * time.Second)
		}
	}

	// if timer is already started, this is a no-op
	startTimer := func() {
		if !ticking {
			ticking = true
			c.logger.Info("In startTimer")
			timer.Reset(c.support.SharedConfig().BatchTimeout())
		}
	}

	submitC := c.submitC
	var bc *blockCreator

	// I added here blockCreator initialization

	c.logger.Infof("Initializing the blockCreator....")

	bc = &blockCreator{
		hash:   protoutil.BlockHeaderHash(c.lastBlock.Header),
		number: c.lastBlock.Header.Number,
		logger: c.logger,
	}

	var propC chan<- *common.Block
	var cancelProp context.CancelFunc
	cancelProp = func() {} // no-op as initial value

	// channel where the block of the proposal phase will be passed
	//blockProposalC := make(chan *common.Block, c.opts.MaxInflightBlocks)

	becomeLeader := func() (chan<- *common.Block, context.CancelFunc) {
		//c.Metrics.IsLeader.Set(1)

		c.blockInflight = 0
		//c.justElected = true
		//submitC = nil
		submitC = c.submitC
		ch := make(chan *common.Block, c.opts.MaxInflightBlocks)

		// if there is unfinished ConfChange, we should resume the effort to propose it as
		// new leader, and wait for it to be committed before start serving new requests.
		//if cc := c.getInFlightConfChange(); cc != nil {
		// The reason `ProposeConfChange` should be called in go routine is documented in `writeConfigBlock` method.
		//	go func() {
		//		if err := c.Node.ProposeConfChange(context.TODO(), *cc); err != nil {
		//			c.logger.Warnf("Failed to propose configuration update to Raft node: %s", err)
		//		}
		//	}()

		//	c.confChangeInProgress = cc
		//	c.configInflight = true
		//}

		// Leader should call Propose in go routine, because this method may be blocked
		// if node is leaderless (this can happen when leader steps down in a heavily
		// loaded network). We need to make sure applyC can still be consumed properly.
		ctx, cancel := context.WithCancel(context.Background())
		go func(ctx context.Context, ch <-chan *common.Block) {
			for {
				select {
				case b := <-ch:
					// TODO Uncomment the code
					data := protoutil.MarshalOrPanic(b)

					c.logger.Infof("I am the leader and i will Propose the block [%d] to raft consensus, right before invoking c.Node.Propose", b.Header.Number)

					if err := c.Node.Propose(ctx, data); err != nil {
						c.logger.Errorf("Failed to propose block [%d] to raft and discard %d blocks in queue: %s", b.Header.Number, len(ch), err)
						return
					}
				case <-ctx.Done():
					c.logger.Infof("Quit proposing blocks, discarded %d blocks in the queue", len(ch))
					return
				}
			}
		}(ctx, ch)

		return ch, cancel
	}

	becomeFollower := func() {
		cancelProp()
		c.blockInflight = 0
		_ = c.support.BlockCutter().Cut()
		//stopTimer()
		submitC = c.submitC
		//bc = nil
		//c.Metrics.IsLeader.Set(0)
	}

	// c.leader is initialized in the c.start function
	if c.leader == 1 {
		c.logger.Infof("Invoking becomeLeader()")
		propC, cancelProp = becomeLeader()
	} else {
		c.logger.Infof("Invoking becomeFollower()")
		becomeFollower()
	}

	// TODO initiliazing the errorC
	c.errorC = make(chan struct{})

	var startTime4MessagesInBlockProposal time.Time
	var startTime4MessagesInVotingPhase time.Time

	// Here waits for a block to be proposed and send it then to everyone else
	ctx4BlockProposal, _ := context.WithCancel(context.Background())
	go func(ctx context.Context, ch <-chan blockProposalStruct) {
		for {
			select {
			case blockProposalSt := <-c.blockProposalC:
				nodeStateString := consensusStateToString(c.Node.state)
				c.logger.Infof("[%s] --> Our own block has been calculated with index %v", nodeStateString, blockProposalSt.blockData.Header.Number)
				data := protoutil.MarshalOrPanic(blockProposalSt.blockData)
				ctx, _ := context.WithCancel(context.Background())
				nodeVRF := blockProposalSt.nodeVRF
				blockNumber := blockProposalSt.blockData.Header.Number
				if err := c.Node.ProposeVRFBlock(ctx, data, nodeVRF, nodeStateString, blockNumber); err != nil {
					c.logger.Infof("[BlockProposal] --> Failed to propose a vrf block: %s", err)

				}
			}

		}
	}(ctx4BlockProposal, c.blockProposalC)

	for {
		select {
		case s := <-submitC:
			c.logger.Infof("got an element from the submitC channel")

			// send the c.leader to the s.leader channel
			// TODO really redundant
			s.leader <- c.leader
			// check if i am the leader or not
			// if I am not the leader, i need to forward the message
			// Submit function will forward it
			if c.leader != 1 {
				continue
			}
			c.logger.Infof("I am the leader and i am about to propose a message")

			batches, pending, err := c.ordered(s.req)
			if err != nil {
				c.logger.Errorf("Failed to order message: %s", err)
				continue
			}

			if !pending && len(batches) == 0 {
				c.logger.Infof("!pending && len_batches == 0, we should not be here")
				continue
			}

			// start the timer if pending == True
			if pending {
				c.logger.Infof("pending = %x so startTimer will be invoked", pending)
				startTimer()
			}

			// TODO care!!!
			c.propose(propC, bc, batches...)

			// timer of the batch timeout

		// Both Consensus and Submit communicate via this channel
		// TODO maybe add a discrete channel with a distinct functionality for the Consensus
		case vrfpairReceived := <-c.vrfC:

			nodeStateString := consensusStateToString(c.Node.state)
			//blockNumberToBeAdded := c.lastBlock.Header.Number + 1

			// current bestVRF encoded into hex in order to be compared with the vrf received
			bestVRFHex := hex.EncodeToString(c.bestVRF.vrf)
			receivedVRFHex := hex.EncodeToString(vrfpairReceived.vrf)

			//c.raftMetadataLock.Lock()
			if nodeStateString != "BlockProposal" {

				c.logger.Infof("[%s] Chain just receive a vrf while not in blockProposal phase", nodeStateString)
			}
			blockNumberToBeAdded := c.lastBlock.Header.Number + 1

			// received a vrfpair from another orderer

			//c.raftMetadataLock.Lock()

			//if vrfpairReceived.vrf > c.bestVRF.vrf && vrfpairReceived.sender != 0 {
			if receivedVRFHex > bestVRFHex && vrfpairReceived.sender != 0 {
				//blockNumberToBeAdded := c.lastBlock.Header.Number + 1

				if vrfpairReceived.blockIndex != blockNumberToBeAdded {
					// TODO probably save it for later
					c.logger.Warnf("warn block proposal for block index = %v while I am about to add block index = %v", vrfpairReceived.blockIndex, blockNumberToBeAdded)
				}

				if vrfpairReceived.blockIndex < blockNumberToBeAdded {
					c.logger.Warnf("warn block proposal for block index = %v while I am about to add block index = %v it will be ignored", vrfpairReceived.blockIndex, blockNumberToBeAdded)
				}

				if vrfpairReceived.blockIndex == blockNumberToBeAdded && nodeStateString == "BlockProposal" {
					c.logger.Infof("[%s] Chain received block proposal for expected block index = %v while on blockProposal phase", nodeStateString, blockNumberToBeAdded)
					c.logger.Infof("[%s] The Chain received better vrf= %v via vrf channel from orderer%v.example.com", nodeStateString, vrfpairReceived.vrf, vrfpairReceived.sender-1)
					c.logger.Infof("[%s] Chain received better vrf = %v for block index = %v", nodeStateString, vrfpairReceived.vrf, blockNumberToBeAdded)
					c.bestVRF.sender = vrfpairReceived.sender
					c.bestVRF.vrf = vrfpairReceived.vrf
					c.bestVRF.blockDataEntries = vrfpairReceived.blockDataEntries
					if len(c.bestVRF.blockDataEntries) == 0 {
						c.logger.Panicf("[%s] Received empty blockDataEntries from another orderer", nodeStateString)
					}
				}

				if vrfpairReceived.blockIndex == blockNumberToBeAdded && nodeStateString != "BlockProposal" {
					c.logger.Infof("[%s] warn Chain received block proposal for expected block index = %v but not in blockProposal phase", nodeStateString, blockNumberToBeAdded)

					c.bestVRF.sender = vrfpairReceived.sender
					c.bestVRF.vrf = vrfpairReceived.vrf
					c.bestVRF.blockDataEntries = vrfpairReceived.blockDataEntries
					if len(c.bestVRF.blockDataEntries) == 0 {
						c.logger.Panicf("[%s] Received empty blockDataEntries from another orderer", nodeStateString)
					}
				}

				//c.logger.Infof("[%s] Sending confirmation for block data entries for index = %v", nodeStateString, blockNumberToBeAdded)
				//c.blockDataEntriesConfirmationC <- struct{}{}

			}

			/*
				if vrfpairReceived.sender != 0 {
					c.logger.Infof("[%s] Sending confirmation for block data entries for index = %v", nodeStateString, blockNumberToBeAdded)
					go func() {
						c.blockDataEntriesConfirmationC <- struct{}{}
					}()
				}
			*/

			// vrfSenderRequest received from peer's gateway
			// blockDataEntries are added in function c.Node
			// carefull this is a latest add
			/*
				txnPayload := string(vrfpairReceived.request.Payload.Payload)
				var alreadyMinted bool = false

				c.pendingVrfPairsLock.Lock()
				if _, exists := c.txnsMinted[txnPayload]; exists {
					alreadyMinted = true
					delete(c.txnsMinted, txnPayload)
				}
				c.pendingVrfPairsLock.Unlock()
			*/

			if vrfpairReceived.sender == 0 {
				//if vrfpairReceived.sender == 0 && !alreadyMinted {

				c.counterOfPeerSubmits++
				// Add the vrf to the c.nodeVRF
				//if vrfpairReceived.vrf != 0 {
				if string(vrfpairReceived.vrf) == "NeedToCalculate" && hex.EncodeToString(c.nodeVRF) == "00" {
					//if string(vrfpairReceived.vrf) == "NeedToCalculate" {
					//c.nodeVRF = vrfpairReceived.vrf
					// Calculate the new vrf using the last seed block
					blockNumberToBeAdded := c.lastBlock.Header.Number + 1
					seed4VRF := c.lastBlock.Header.Seed
					c.logger.Infof("[%s]run We will calculate a vrf for block index = %v with seed = %v provided from last block header seed", nodeStateString, blockNumberToBeAdded, seed4VRF)

					_, pi, err := ecvrf.P256Sha256Tai.Prove(c.Node.privateKeyTLS, seed4VRF)
					if err != nil {
						c.logger.Panicf("[%s]Error in creating vrf: %v", nodeStateString, err)
					}
					c.logger.Infof("[%s] calculated vrf = %v from seed = %v for blockindex = %v", nodeStateString, pi, seed4VRF, blockNumberToBeAdded)
					c.nodeVRF = pi
					vrfpairReceived.vrf = pi

					// Sanity check with our publicKey
					c.logger.Infof("[%s] My raftID = %v and c.orderersPublicKeys[%v] = %v", nodeStateString, c.raftID, c.raftID, c.orderersPublicKeys[c.raftID])
					_, err = ecvrf.P256Sha256Tai.Verify(c.orderersPublicKeys[c.raftID], seed4VRF, pi)
					if err != nil {
						c.logger.Infof("Sanity check of vrf has an error = %v", err)
					} else {
						c.logger.Infof("Sanity check of vrf is ok ")
					}

				} else {
					zeroHex, err := hex.DecodeString("00")
					if err != nil {
						c.logger.Panicf("Error in Decoding zero string: %v", err)
					}
					vrfpairReceived.vrf = zeroHex
				}

				//c.raftMetadataLock.Lock()
				vrfHexReceived := hex.EncodeToString(vrfpairReceived.vrf)
				//if vrfpairReceived.vrf > c.bestVRF.vrf {
				if vrfHexReceived > bestVRFHex {
					//c.raftMetadataLock.Lock()
					c.bestVRF.sender = vrfpairReceived.sender
					c.bestVRF.vrf = vrfpairReceived.vrf
					//	c.raftMetadataLock.Unlock()
					c.logger.Infof("[%s] Current best vrf is calculated by us: %v", nodeStateString, vrfpairReceived.vrf)
				} //else {
				//	c.raftMetadataLock.Unlock()
				//}

				//c.raftMetadataLock.Unlock()

				requestReceived := vrfpairReceived.request

				// carefull this is a latest add

				// TODO added the && not sure if it is ok
				c.logger.Infof("[%s] This the %vth number of submit received from a peer's gateway", nodeStateString, c.counterOfPeerSubmits)
				if c.blockInflight < 1 && !c.processingABlock {
					c.logger.Infof("[%s] --> Order the message with c.ordered", nodeStateString)

					// add the request to the map
					txn := requestReceived.Payload
					//c.txnsInFlight[txn] = requestReceived
					c.txnsInFlight[string(txn.Payload)] = requestReceived

					// starting the counter for the blockproposal phase duration
					//startBlockProposalPhaseTimer()

					// timer added in order to check how much time elapsed until receiving quorum messages
					startTime4MessagesInBlockProposal = time.Now()

					batches, pending, err := c.ordered(requestReceived)

					c.logger.Infof("[%s] -- after ordered pending = %v, batches = %v", nodeStateString, pending, len(batches))

					if err != nil {
						c.logger.Errorf("[%s] Failed to order message: %s", nodeStateString, err)
					}
					if pending == true && len(batches) == 1 {
						//c.logger.Warnf("[%s] -- Why am i here?????", nodeStateString)
						c.logger.Panicf("[%s] -- Why am i here?????", nodeStateString)
					}
					if pending == false && len(batches) != 0 {
						c.logger.Infof("[%s] -- The batch size of block is reached so we will propose it now index = %v", nodeStateString, blockNumberToBeAdded)
						c.batches = append(c.batches, batches...)
						c.processingABlock = true
						blockProposalTimer.Reset(0 * time.Second)
					}
					if !pending && len(batches) == 0 {
						c.logger.Infof("[BlockProposal] --> !pending && len_batches == 0, we should not be here")
						continue
					}

					// start the timer if pending == True
					if pending {
						c.logger.Infof("[BlockProposal] --> pending = %v so startTimer will be invoked", pending)
						startBlockProposalTimer()
					}
				} else {
					c.logger.Infof("[%s] --> c.blockInflight != 0 so the txn will not make it in this block it will be cached for the next one", nodeStateString)
					zeroHex, err := hex.DecodeString("00")
					if err != nil {
						c.logger.Panicf("Error in Decoding zero string: %v", err)
					}
					// cache the txn to be added to the next block
					vrfpair := vrfSenderRequest{
						vrf:     zeroHex,
						sender:  vrfpairReceived.sender,
						request: requestReceived,
					}

					c.pendingVrfPairsLock.Lock()
					//c.pendingVrfPairs = append(c.pendingVrfPairs, vrfpair)
					c.pendingVrfPairs[string(requestReceived.Payload.Payload)] = vrfpair
					c.pendingVrfPairsLock.Unlock()

				}
			}

		case <-blockProposalTimer.C():
			nodeStateString := consensusStateToString(c.Node.state)

			ticking = false
			c.logger.Infof("[%s] Time proposal is triggered we are going to create the block", nodeStateString)

			// added now be careful!!!
			//c.processingABlock = true

			var batch []*cb.Envelope
			if len(c.batches) == 0 {
				batch = c.support.BlockCutter().Cut()
				if len(batch) == 0 {
					c.logger.Warningf("[%s]Batch timer expired with no pending requests, this might indicate a bug", nodeStateString)
					continue
				}
			} else {
				batch = c.batches[0]
				c.batches = c.batches[1:]

			}
			c.logger.Infof("[%s] --> blockProposalTimer expired, creating the block to propose current Index = %d", nodeStateString, c.lastBlock.Header.Number+1)
			bc.number = c.lastBlock.Header.Number
			bc.hash = protoutil.BlockHeaderHash(c.lastBlock.Header)
			//c.createTheBlockToPropose(c.blockProposalC, bc, batch)
			c.createTheBlockToPropose(c.blockProposalC, bc, batch)
			blockNumberToBeAdded := c.lastBlock.Header.Number + 1

			// check if i have a quorum block proposals from a previous round ....
			if _, exists := c.quorumBlockProposals[blockNumberToBeAdded]; exists {
				c.logger.Infof("[%s] quorum of block proposals for block index = %v has already received", nodeStateString, blockNumberToBeAdded)
				c.logger.Infof("[%s] Signaling the c.blockProposalPhaseC channel", nodeStateString)
				go func() {
					c.blockProposalPhaseC <- struct{}{}
				}()
			} else {
				//	blockNumberToBeAdded := c.lastBlock.Header.Number + 1
				go func() {
					timePassed := false
					for !timePassed {
						select {
						case blkIndex := <-c.quorumProposals4IndexC:
							if blkIndex == blockNumberToBeAdded {
								c.logger.Infof("[%s] Received quorum block proposals from all orderers with blkIndex = %v as expected", nodeStateString, blkIndex)
								timePassed = true
							} else if blkIndex < blockNumberToBeAdded {
								c.logger.Warnf("[%s] Received quorum block proposals for previous block index = %v despite expecting %v discarding it....", nodeStateString, blkIndex, blockNumberToBeAdded)
								//c.quorumBlockProposals[blkIndex] = true
							} else {
								c.logger.Warnf("[%s] Received quorum block proposals for future block index = %v despite expecting %v saving it ....", nodeStateString, blkIndex, blockNumberToBeAdded)
								c.quorumBlockProposals[blkIndex] = true
							}
						case <-time.After(time.Second * 7):
							//	c.logger.Infof("[%s] No block proposals received from all orderers", nodeStateString)
							c.logger.Warnf("[%s] Not Received quorum block proposals from all orderers for block index = %v after timeOut", nodeStateString, blockNumberToBeAdded)
							timePassed = true
						}
					}
					c.logger.Infof("[%s] Signaling the c.blockProposalPhaseC channel", nodeStateString)

					//go func() {
					c.blockProposalPhaseC <- struct{}{}
				}()
			}

			//case <-blockProposalPhaseTimer.C():
		case <-c.blockProposalPhaseC:
			nodeStateString := consensusStateToString(c.Node.state)
			blockNumberToBeAdded := c.lastBlock.Header.Number + 1
			// check if our VRF has been elected
			//if hex.EncodeToString(c.ownVRF) == hex.EncodeToString(c.bestVRF.vrf) {
			if hex.EncodeToString(c.ownVRF) >= hex.EncodeToString(c.bestVRF.vrf) {
				c.logger.Infof("[%s] Best vrf after block proposal is %v by us for index = %v", nodeStateString, hex.EncodeToString(c.bestVRF.vrf), blockNumberToBeAdded)
				c.bestVRF.blockDataEntries = c.ownBlockDataEntries
				if len(c.ownBlockDataEntries) == 0 {
					c.logger.Panicf("own Data Entries is nil!!!!!!!!!!!! for block index = %v", blockNumberToBeAdded)
				}
				c.bestVRF.sender = 0
			} else {
				c.logger.Infof("Best vrf after block proposal is %v for index = %v", hex.EncodeToString(c.bestVRF.vrf), blockNumberToBeAdded)
				if len(c.ownBlockDataEntries) == 0 {
					c.logger.Panicf("own Data Entries length is nil!!!!!!!!!!!!")
				}
			}

			// added now
			if len(c.bestVRF.blockDataEntries) == 0 {
				c.logger.Panicf("omg best block data entries is 0 for index = %v", blockNumberToBeAdded)
			}

			//c.logger.Infof("Sending confirmation of ownDataEntries calculated for blockIndex = %v via ownBlockDataC", blockNumberToBeAdded)
			//go func() {
			//	c.ownBlockDataC <- blockNumberToBeAdded
			//}()

			//blockProposalPhaseTicking = false
			//nodeStateString := consensusStateToString(c.Node.state)
			//c.logger.Infof("[%s] --> End of blockProposalTimer we are transitioning to the Voting phase after %v ", nodeStateString, time.Since(startTime4MessagesInBlockProposal).Seconds())
			startTime4MessagesInVotingPhase = time.Now()
			select {
			case <-c.blockSentToEveryoneC:
				c.logger.Infof("[%s] -- Block Proposal is sent to everyone going to voting next phase after time: %v", nodeStateString, time.Since(startTime4MessagesInBlockProposal).Seconds())
			// TODO i have changed it from 1 to 2
			case <-time.After(time.Second * 2):
				c.logger.Infof("[%s] -- Block Proposal could not be sent to every node after time: %v", nodeStateString, time.Since(startTime4MessagesInBlockProposal).Seconds())
				//TODO roll back node's state
			}

			c.logger.Infof("[%s] End of block proposal phase transitioning to the voting phase...", nodeStateString)
			c.Node.state = VotingPhase
			nodeStateString = consensusStateToString(c.Node.state)
			c.logger.Infof("[%s] Starting VotingPhase", nodeStateString)
			go func() {
				c.votingPhaseStartC <- struct{}{}
			}()

			//case <-c.votingPhaseTimer.C():
		case <-c.votingPhaseStartC:

			nodeStateString := consensusStateToString(c.Node.state)

			c.logger.Infof("[%s] VotingPhaseStart channel got signal ", nodeStateString)
			c.logger.Infof("[%s] --> Node's reputation is: %v", nodeStateString, c.reputationNodes[c.raftID])
			isReputable := c.reputationNodes[c.raftID] > 0.5
			blockNumberToBeAdded := c.lastBlock.Header.Number + 1

			//blockIndexFromDataEntriesChannel := <-c.ownBlockDataC
			//c.logger.Infof("Received confirmation for ownDataEntries for blockIndex = %v via ownBlockDataC", blockIndexFromDataEntriesChannel)

			//c.logger.Infof("waiting data entries confirmation for index = %v", blockNumberToBeAdded)
			//_ = <-c.blockDataEntriesConfirmationC
			//c.logger.Infof("In Consensus received data entries confirmation for index = %v", blockNumberToBeAdded)

			// orderer checks if it is a reputable node
			// in case of a reputable node votes need to be sent to every other orderer of the network
			if isReputable {
				c.logger.Infof("[%s] --> Node is a reputable node going to send vote to every other orderer", nodeStateString)

				// TODO we need a lock...................................................................

				vrf := c.bestVRF.vrf
				c.logger.Infof("[%s] --> About to read blockDataEntries", nodeStateString)

				//c.raftMetadataLock.Lock()
				if len(c.bestVRF.blockDataEntries) == 0 {
					c.logger.Panicf("[%s] --> blockDataEntries length == 0 why??????????? for index = %v", nodeStateString, blockNumberToBeAdded)
				}
				data := c.bestVRF.blockDataEntries[0].Data
				//c.raftMetadataLock.Unlock()

				c.logger.Infof("[%s] --> After reading blockDataEntries", nodeStateString)
				ctx, _ := context.WithCancel(context.Background())
				if err := c.Node.ProposeVRFBlock(ctx, data, vrf, nodeStateString, blockNumberToBeAdded); err != nil {
					c.logger.Infof("[%s] --> Failed to propose a vrf block: %s", nodeStateString, err)
				}

			} else {
				c.logger.Infof("[%s] --> Node is not a reputable node not going to vote", nodeStateString)
			}

			//var vrfIndex float32
			var vrfIndex []byte

			// First Check if there is already a VRF for the specified blockIndex
			if _, exists := c.reachedQuorum4Index[blockNumberToBeAdded]; exists {
				c.logger.Infof("[%s] Block with vrf = %v has reached majority votes from previous round")
				vrfIndex = c.reachedQuorum4Index[blockNumberToBeAdded].vrfIndex
			} else {
				// we will wait via channel to get quorum for 4 seconds
				timePassed := false
				for !timePassed {
					select {
					case quorumResult := <-c.quorum4IndexC:
						//c.logger.Infof("[%s] quorum votes reached for block number = %v with vrf = %v", nodeStateString, quorumResult.blockIndex, quorumResult.vrfIndex)
						blockIndexReceived := quorumResult.blockIndex

						if blockIndexReceived != blockNumberToBeAdded {
							c.logger.Infof("[%s] quorum votes not reached for block index = %v with vrf = %v != %v, saving it for later...", nodeStateString, quorumResult.blockIndex, quorumResult.vrfIndex, blockNumberToBeAdded)
							c.reachedQuorum4Index[blockIndexReceived] = quorumResult
						} else {
							c.logger.Infof("[%s] quorum votes reached for block index = %v with vrf = %v", nodeStateString, quorumResult.blockIndex, quorumResult.vrfIndex)
							c.reachedQuorum4Index[blockIndexReceived] = quorumResult
							vrfIndex = quorumResult.vrfIndex
							timePassed = true
						}

					case <-time.After(time.Second * 7):
						c.logger.Infof("[%s] No quorum votes reached for current block index = %v", nodeStateString, blockNumberToBeAdded)
						timePassed = true

						//c.removePotentialDupTxns()
						// TODO check where are we with the support call

						c.pendingVrfPairsLock.Lock()
						for _, request4Txn := range c.txnsInFlight {
							nodeVRF, _ := hex.DecodeString("00")
							vrfpair := vrfSenderRequest{
								vrf:     nodeVRF,
								sender:  0,
								request: request4Txn,
							}

							c.pendingVrfPairs[string(request4Txn.Payload.Payload)] = vrfpair

						}
						c.pendingVrfPairsLock.Unlock()

						c.DeleteStructsAndReset()

						c.CheckIfPendingTxns()

					}
				}
			}

			if isReputable {
				// all votes have been succesfully sent to every other orderer
				select {
				case <-c.blockVotesToEveryoneC:
					c.logger.Infof("[%s] -- block votes sent to everyone after %v time elapsed since voting phase", nodeStateString, time.Since(startTime4MessagesInVotingPhase).Seconds())
				case <-time.After(time.Second * 3):
					c.logger.Infof("[%s] -- block votes could not be sent to everyone after %v time elapsed since voting phase: %v", nodeStateString, time.Since(startTime4MessagesInVotingPhase).Seconds())
				}
			}

			//c.quorumVotes = false
			c.logger.Infof("[%s] --> //////////////////////// VotingPhase has come to an end after time: %v ///////////////////////////////////", nodeStateString, time.Since(startTime4MessagesInVotingPhase).Seconds())

			// check if quorum is reached
			//if vrfIndex > 0 {
			if vrfIndex != nil {

				c.votedVRF = vrfIndex

				c.Node.state = CommitPhase
				nodeStateString = consensusStateToString(c.Node.state)

				// here we check that the vrf is actually correct

				//c.raftMetadataLock.Lock()
				winnerSender := c.bestVRF.sender
				//c.raftMetadataLock.Unlock()

				if winnerSender == 0 {
					c.logger.Infof("[%s] Our own vrf is chosen after voting phase...", nodeStateString)
				} else {
					c.logger.Infof("[%s] Going to check the vrf = %v from sender = %v which is orderer%v.example.com", nodeStateString, c.bestVRF.vrf, c.bestVRF.sender, c.bestVRF.sender-1)
					winnerPubKeyHex := c.orderersPublicKeys[winnerSender]
					secretMsg := c.lastBlock.Header.Seed
					c.logger.Infof("[%s] We will check the vrf = %v if it is generated from seed = %v with PublicKey = %v", nodeStateString, c.bestVRF.vrf, secretMsg, winnerPubKeyHex)
					_, err := ecvrf.P256Sha256Tai.Verify(winnerPubKeyHex, secretMsg, c.bestVRF.vrf)
					if err != nil {
						c.logger.Panicf("[%s] block = %v vrf is not produced from sender = %v or has seed = %v", nodeStateString, blockNumberToBeAdded, winnerSender, secretMsg)
					} else {
						c.logger.Infof("[%s] block = %v, vrf is produced from sender = %v and seed = %v", nodeStateString, blockNumberToBeAdded, winnerSender, secretMsg)
					}

				}
				//c.raftMetadataLock.Unlock()

				go func() {
					c.logger.Infof("[%s] --> applying changes with vrf = %v", nodeStateString, vrfIndex)

					//c.raftMetadataLock.Lock()

					//blockData := c.votingBlocks[blockNumberToBeAdded][vrfIndex].entries
					blockData := c.reachedQuorum4Index[blockNumberToBeAdded].entries
					c.applyC <- apply{blockData}

					//c.raftMetadataLock.Unlock()

				}()
			} else {
				c.logger.Infof("[%s] quorum votes not reached for block index = %v", nodeStateString, blockNumberToBeAdded)
			}

		case <-timer.C():
			ticking = false
			c.logger.Infof("Timer triggered so we are going to propose the block which has no batches")

			batch := c.support.BlockCutter().Cut()
			if len(batch) == 0 {
				c.logger.Warningf("Batch timer expired with no pending requests, this might indicate a bug")
				continue
			}

			c.logger.Infof("Batch timer expired, creating block")
			c.propose(propC, bc, batch) // we are certain this is normal block, no need to block
		case app := <-c.applyC:

			nodeStateString := consensusStateToString(c.Node.state)

			if c.Node.state == CommitPhase {
				c.logger.Infof("[%s] --> applying changes ......", nodeStateString)
			}
			c.apply(app.entries)
			if c.Node.state == CommitPhase {
				c.logger.Infof("[%s] --> /////////////// Finished after  block proposal has started %v ///////////////", nodeStateString, time.Since(startTime4MessagesInBlockProposal).Seconds())

				//c.DeleteStructsAndReset()

				//txnsToProposeAgain := c.removePotentialDupTxns()
				c.removePotentialDupTxns()

				c.DeleteStructsAndReset()

				//c.CheckIfPendingTxns(txnsToProposeAgain)
				c.CheckIfPendingTxns()

				//c.logger.Infof("[%s] --> Deleting votingBlocks and make nil the bestVRF", nodeStateString)

				// delete all votes for the current block index
				/*
					c.raftMetadataLock.Lock()
					delete(c.votingBlocks, blockNumberToBeAdded)
					delete(c.blockProposals, blockNumberToBeAdded)
					c.bestVRF = vrfSenderRequest{}
					c.raftMetadataLock.Unlock()
					c.logger.Infof("[%s] --> Deleting c.reachedQuorum4Index for block num = %v", nodeStateString, blockNumberToBeAdded)
					delete(c.reachedQuorum4Index, blockNumberToBeAdded)
				*/

				/*
					var err error
					c.nodeVRF, err = hex.DecodeString("00")

					if err != nil {
						c.logger.Panicf("[%s] Error in decoding zeroHex string: %v", nodeStateString, err)
					}
					c.ownVRF, _ = hex.DecodeString("00")
				*/

				// delete quorumVotes for the current block index
				//c.quorumVotesLock.Lock()
				//delete(c.quorumVotes, blockNumberToBeAdded)
				//c.quorumVotesLock.Unlock()

				//c.logger.Infof("[%s] Going to check if there are any pendingVrfPairs in the queue ... ", consensusStateToString(c.Node.state))
				//c.logger.Infof("[%s] c.blockInflight = %v ", consensusStateToString(c.Node.state), c.blockInflight)

				/*
					c.logger.Infof("[%s] Setting c.calculatedVRF back to false....", nodeStateString)
					c.calculatedVRFLock.Lock()
					c.calculatedVRF = false
					c.calculatedVRFLock.Unlock()
				*/

				//c.Node.state = BlockProposal
				nodeStateString = consensusStateToString(c.Node.state)
				c.logger.Infof("[%s] --> /////////////////// Going back to block proposal after time %v//////////////////", nodeStateString, time.Since(startTime4MessagesInBlockProposal).Seconds())

				// TODO check it
				//c.processingABlock = false

				/*
					if len(c.pendingVrfPairs) != 0 {
						c.logger.Infof("[%s] --> There are pending txns length = %v, so they need to be included in the current block ....", nodeStateString, len(c.pendingVrfPairs))

						// critical section

						//go func() {
						c.pendingVrfPairsLock.Lock()
						var txnsExtracted = 0
						for _, pendingPair := range c.pendingVrfPairs {
							//requestPending := pendingPair.request
							var reqPending orderer.SubmitRequest
							reqPending = *pendingPair.request
							senderOfPendingRequest := pendingPair.sender

							// invoking c.Submit for the pending txn
							c.logger.Infof("[%s] I am about to invoke a pending pair", nodeStateString)
							//go c.Submit(requestPending, senderOfPendingRequest)
							go c.Submit(&reqPending, senderOfPendingRequest)
							txnsExtracted++
							if uint32(txnsExtracted) == c.batchSize {
								c.logger.Infof("[%s] I extracted %v txns from the pendingPair queue", nodeStateString, c.batchSize)
								break
							}

						}
						c.pendingVrfPairs = c.pendingVrfPairs[txnsExtracted:]
						c.pendingVrfPairsLock.Unlock()
					} else {
						c.logger.Infof("[%s] There are not any pending txns...", nodeStateString)
					}
				*/
			}

		}

	}
}

// Order submits normal type transactions for ordering.
func (c *Chain) Order(env *common.Envelope, configSeq uint64) error {
	// TODO uncomment the return which uses the Submit
	//c.Metrics.NormalProposalsReceived.Add(1)
	// here we will increase counter
	c.logger.Infof("-----------c.Order INVOCATION------------------")
	//c.counter = c.counter + 1
	return c.Submit(&orderer.SubmitRequest{LastValidationSeq: configSeq, Payload: env, Channel: c.channelID}, 0)
	//return nil
}

// WaitReady blocks when the chain:
// - is catching up with other nodes using snapshot
//
// In any other case, it returns right away.
func (c *Chain) WaitReady() error {
	// TODO uncomment it
	/*
		if err := c.isRunning(); err != nil {
			return err
		}

		select {
		case c.submitC <- nil:
		case <-c.doneC:
			return errors.Errorf("chain is stopped")
		}
	*/

	return nil
}

// Errored returns a channel that closes when the chain stops.
func (c *Chain) Errored() <-chan struct{} {
	// TODO uncomment the following lines
	//c.errorCLock.RLock()
	//defer c.errorCLock.RUnlock()

	return c.errorC
}

// Submit forwards the incoming request to:
// - the local run goroutine if this is leader
// - the actual leader via the transport mechanism
// The call fails if there's no leader elected yet.
func (c *Chain) Submit(req *orderer.SubmitRequest, sender uint64) error {
	// TODO uncomment
	nodeStateString := consensusStateToString(c.Node.state)

	c.logger.Info("xxxx----------------------------xxxxxx")
	c.logger.Infof("[%s] I am inside the c.Submit function handling a Submit request", nodeStateString)
	c.logger.Infof("[%s] VRF inside the request is : %v", nodeStateString, req.Vrf)

	if err := c.isRunning(); err != nil {
		// TODO I have commented out metrics
		//c.Metrics.ProposalFailures.Add(1)
		return err
	}

	//c.logger.Infof("orderer.SubmitRequest")
	//spew.Dump(req)

	maxMessageCount := c.support.SharedConfig().BatchSize
	c.logger.Infof("[%s] maxMessageCount = %v", nodeStateString, maxMessageCount)

	if c.opts.RaftID == 3 {
		c.counter = c.counter + 1
	}
	c.logger.Infof("----c.counter = %d-----", c.counter)

	// Check if this txn has already been minted...

	c.pendingVrfPairsLock.Lock()
	txnPayload := string(req.Payload.Payload)
	if _, exists := c.txnsMinted[txnPayload]; exists {
		delete(c.txnsMinted, txnPayload)
		c.pendingVrfPairsLock.Unlock()
		return nil
	} else {
		c.pendingVrfPairsLock.Unlock()
	}

	// we are in the proposal state and we have got a message directly from a peer
	//if c.Node.state == BlockProposal && sender == 0 {
	if nodeStateString == "BlockProposal" && sender == 0 {
		c.logger.Infof("[%s] --> Got a Submit request from the peer containing a transaction.......", nodeStateString)

		// Need to check if i am going to calculate a vrf

		var vrfpair vrfSenderRequest
		//var nodeVRF float32
		var nodeVRF []byte

		// Checking if we have already calculated a VRF for the current block proposal phase
		c.calculatedVRFLock.Lock()
		if !c.calculatedVRF {

			c.calculatedVRF = true
			//c.nodeVRF, _ = hex.DecodeString("00")
			nodeVRF = []byte("NeedToCalculate")
			vrfpair = vrfSenderRequest{
				vrf:     nodeVRF,
				sender:  sender,
				request: req,
			}

			c.logger.Infof("[%s] --> Our own VRF calculated is: %f", nodeStateString, nodeVRF)

		} else {
			//nodeVRF = float32(0)
			nodeVRF := []byte("Calculated")
			vrfpair = vrfSenderRequest{
				vrf:     nodeVRF,
				sender:  sender,
				request: req,
			}
			c.logger.Infof("[%s] --> A VRF is already calculated we will send the ZERO value VRF: %v", nodeStateString, nodeVRF)
		}
		c.calculatedVRFLock.Unlock()

		select {

		case c.vrfC <- vrfpair:
			c.logger.Infof("[%s] Sending our vrf via the c.vrfC channel ", nodeStateString)

		case <-c.doneC:
			return errors.Errorf("chain is stopped")
		}

	} else if c.Node.state == BlockProposal && sender != 0 {
		//c.logger.Infof("[BLOCKPROPOSAL] --> WHY ??I just got vrf with value: %f from node = %d", req.Vrf, sender)
		c.logger.Warnf("[%s] --> Bug probably... I just got a request via Submit vrf with value: %f from orderer%d", nodeStateString, req.Vrf, sender-1)
	} else if c.Node.state == PreConsesusState {

		//if c.Node.state == PreConsesusState {
		leadC := make(chan uint64, 1)
		select {
		case c.submitC <- &submit{req, leadC}:

			// checks if it is the leader or not
			lead := <-leadC
			// I am not the leader i need to forward it
			if lead != 1 {
				// TODO uncomment it
				// TODO lead is hardcoded please do not forget to change it
				//if err := c.forwardToLeader(lead, req); err != nil {
				if err := c.forwardToLeader(3, req); err != nil {
					return err
				}
			}
		case <-c.doneC:
			//c.Metrics.ProposalFailures.Add(1)
			return errors.Errorf("chain is stopped")

		}

		if c.counter == 3 && c.opts.RaftID == 3 {

			//c.logger.Info("[BLOCKPROPOSAL] --> Going into BlockProposal State")
			//		c.logger.Panic("I am in Submit function panicking because counter = 4")
			c.Node.state = BlockProposal
			c.logger.Infof("[%s] --> Going into BlockProposal State from PreConsesusState", consensusStateToString(c.Node.state))

			// initialize orderersPublicKeys map
			for raftID, consenter := range c.opts.Consenters {
				clientCert, _ := pem.Decode(consenter.ClientTlsCert)
				certPublicKeyCertificate, err := x509.ParseCertificate(clientCert.Bytes)
				if err != nil {
					c.logger.Panicf("Error in parsing certificate = %v", err)
				}
				certPubKey := certPublicKeyCertificate.PublicKey.(*ecdsa.PublicKey)
				certPubKeyHex := hex.EncodeToString(elliptic.Marshal(certPubKey, certPubKey.X, certPubKey.Y))
				c.logger.Infof("raftID = %v PublicKey = %v", raftID, certPubKeyHex)

				// add to orderersPublicKeys
				//c.orderersPublicKeys[raftID] = certPubKeyHex
				c.orderersPublicKeys[raftID] = certPubKey

				// TODO remove it after only for debugging
				c.logger.Infof("c.orderersPublicKeys[%v] = %v", raftID, certPubKeyHex)

			}

			// print and store my privatekey in tls
			filePath := "/var/hyperledger/orderer/tls/server.key"
			dataPrivateKey, err := os.ReadFile(filePath)
			if err != nil {
				c.logger.Panicf("An error occured during read of the private key file --> %v", err)
			}
			pemBlock, _ := pem.Decode(dataPrivateKey)
			priv, e := x509.ParsePKCS8PrivateKey(pemBlock.Bytes)
			privateKey := priv.(*ecdsa.PrivateKey)
			if e != nil {
				c.logger.Panicf("error in ParsePKCS8PrivateKey = %v", e)
			}
			c.logger.Infof("Our tls private key is = %v", privateKey)
			c.Node.privateKeyTLS = privateKey

		}
	} else {
		// Not block Proposal state
		// TODO cache the transaction received, if not in BlockProposal state
		if sender == 0 {
			c.logger.Infof("[%s] I received a Submit but i am not in the blockProposal state so i need to cache the txn for later ... ", nodeStateString)
			//nodeVRF := float32(0)
			nodeVRF, err := hex.DecodeString("00")
			if err != nil {
				c.logger.Panicf("[%s] error in DecodeString from zeroHex: ", nodeStateString, err)
			}
			vrfpair := vrfSenderRequest{
				vrf:     nodeVRF,
				sender:  sender,
				request: req,
			}

			c.pendingVrfPairsLock.Lock()
			if _, exists := c.txnsMinted[txnPayload]; exists {
				delete(c.txnsMinted, txnPayload)
				c.pendingVrfPairsLock.Unlock()
			} else {
				c.pendingVrfPairs[string(req.Payload.Payload)] = vrfpair
				c.pendingVrfPairsLock.Unlock()
				c.logger.Infof("[%s] Cached it successfully .....", nodeStateString)
			}

		} else {
			// sender is not the peer's gateway
			c.logger.Warnf("[%s] --> Bug probably.. I just got vrf with value: %f from orderer%d, probably something went really wrong", nodeStateString, req.Vrf, sender-1)
		}
	}

	return nil
}

func (c *Chain) checkIfQuorumVotes4Index(blockIndex uint64, vrf []byte, votes int) bool {
	c.logger.Infof("[%s] Inside checkIfQuorumVotes4Index ", "Voting")
	if (c.reputationNodes[c.raftID] < 0.5 && votes == majorityOfReputables) || (c.reputationNodes[c.raftID] >= 0.5 && votes == majorityOfReputables-1) {
		c.logger.Infof("[%s] -- Block with vrf = %f has reached the majority votes", consensusStateToString(c.Node.state), hex.EncodeToString(vrf))
		return true
	} else {
		c.logger.Infof("[%s] Inside checkIfQuorumVotes4Index in else why", "Voting")
		return false
	}
}

// Consensus passes the given ConsensusRequest message to the raft.Node instance
func (c *Chain) Consensus(req *orderer.ConsensusRequest, sender uint64) error {

	nodeStateString := consensusStateToString(c.Node.state)

	// Check that i can retrieve the correct last block from here
	//currentHeight := c.support.Height()
	//currentBlock := c.support.Block(currentHeight)
	//c.logger.Infof("[%s] Current Height is %v and last block seed = %v", nodeStateString, currentHeight, currentBlock.Header.Seed)

	if c.Node.state != PreConsesusState {
		// First parse the request Payload to a stepMsg once ...
		stepMsg := &raftpb.Message{}
		if err := proto.Unmarshal(req.Payload, stepMsg); err != nil {
			return fmt.Errorf("failed to unmarshal StepRequest payload to Raft Message: %s", err)
		}

		// The received block index from the orderer
		var receivedBlockIndex uint64
		var receivedState string
		//var receivedVRF float32
		var receivedVRF []byte
		var receivedVRFString string
		//var blockReceived *common.Block
		var entries []raftpb.Entry

		receivedState = req.State.String()
		receivedVRF = req.Vrf
		receivedVRFString = hex.EncodeToString(receivedVRF)
		entries = stepMsg.Entries

		// Check the block's index at first
		for i := range stepMsg.Entries {
			switch stepMsg.Entries[i].Type {
			case raftpb.EntryNormal:
				blockReceived := protoutil.UnmarshalBlockOrPanic(stepMsg.Entries[i].Data)

				receivedBlockIndex = blockReceived.Header.Number

				// This is also edited from another goroutine so it is not safe
				currentBlockIndex := c.lastBlock.Header.Number

				c.logger.Infof("[%s] Received block with vrf = %f, index = %d, our current index = %d", nodeStateString, receivedVRFString, receivedBlockIndex, currentBlockIndex)
				if receivedBlockIndex-1 == currentBlockIndex {
					c.logger.Infof("[%s] Block index received is accepted", nodeStateString)
				} else if receivedBlockIndex <= currentBlockIndex {
					c.logger.Warnf("[%s] Consensus We are not accepting blocks with index less than or equal with %d, block index received = %d", nodeStateString, currentBlockIndex, receivedBlockIndex)
				} else { // check if the index received is bigger than the current so it needs to be cached for later ???
					// TODO what do i do here ???????
					c.logger.Warnf("[%s] Consensus Why am i here ?????????? received block with index %d but my current index is %d", nodeStateString, receivedBlockIndex, currentBlockIndex)
				}

			}
		}

		//if c.Node.state == VotingPhase {
		if receivedState == "VotingPhase" {
			c.logger.Infof("[%s] --> Consensus received block from orderer%d with vrf = %v for phase = %s with block index = %d", nodeStateString, sender-1, receivedVRFString, receivedState, receivedBlockIndex)

			// Critical section needs a lock
			c.raftMetadataLock.Lock()
			if _, ok := c.votingBlocks[receivedBlockIndex]; !ok {
				if _, exists := c.votingBlocks[receivedBlockIndex][receivedVRFString]; !exists {
					c.votingBlocks[receivedBlockIndex] = make(map[string]blockAndVotes)
					c.votingBlocks[receivedBlockIndex][receivedVRFString] = blockAndVotes{entries: stepMsg.Entries, votes: 1}
				}
			} else { // blockIndex is initialized
				// checking if VRF is initialized in blockIndex
				if _, exists := c.votingBlocks[receivedBlockIndex][receivedVRFString]; !exists {
					c.votingBlocks[receivedBlockIndex] = make(map[string]blockAndVotes)
					c.votingBlocks[receivedBlockIndex][receivedVRFString] = blockAndVotes{entries: stepMsg.Entries, votes: 1}
				} else {
					blockVote := c.votingBlocks[receivedBlockIndex][receivedVRFString]
					blockVote.votes = blockVote.votes + 1
					c.votingBlocks[receivedBlockIndex][receivedVRFString] = blockVote
				}
			}

			votesTillNow := c.votingBlocks[receivedBlockIndex][receivedVRFString].votes
			c.logger.Infof("[%s] votesTillNow = %v", "Voting", votesTillNow)
			c.raftMetadataLock.Unlock()

			quorumReached4blockIndex := c.checkIfQuorumVotes4Index(receivedBlockIndex, receivedVRF, votesTillNow)
			if len(entries) == 0 {
				c.logger.Panicf("ERRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR for block index = %v", receivedBlockIndex)
			}
			if quorumReached4blockIndex {
				c.logger.Infof("[%s] Sending quorumResult via the channel with vrf = %v and block index = %v", "Voting", receivedVRF, receivedBlockIndex)
				blockAndVRFToSend := blockAndVRF{
					vrfIndex:   receivedVRF,
					entries:    entries,
					blockIndex: receivedBlockIndex,
				}
				c.quorum4IndexC <- blockAndVRFToSend
			}

		}
		//if c.Node.state == BlockProposal {
		if receivedState == "BlockProposal" {

			// added this goroutine to test c.support.Block() functionality
			//go func() {
			//	retrievedBlock := c.support.Block(c.support.Height() - 1)
			//	c.logger.Infof("[%s] I am a goroutine in Consensus and retrieved last block from support with number = %v", req.State.String(), retrievedBlock.Header.Number)
			//}()

			c.raftMetadataLock.Lock()
			receivedBlockProposals := false
			if _, exists := c.blockProposals[receivedBlockIndex]; !exists {
				c.blockProposals[receivedBlockIndex] = 1
			} else {
				c.blockProposals[receivedBlockIndex]++
				if c.blockProposals[receivedBlockIndex] == len(c.Node.metadata.ConsenterIds)-1 {
					c.logger.Infof("[%s] receivedBlockProposals setting true for block index = %v", nodeStateString, receivedBlockIndex)
					receivedBlockProposals = true
				}
			}
			c.raftMetadataLock.Unlock()

			submitReq := &orderer.SubmitRequest{Vrf: req.Vrf}
			//c.logger.Infof("[%s] --> Consensus received block with vrf: %f from orderer %d for phase = %s for index = %d", nodeStateString, req.Vrf, sender-1, req.State.String(), c.lastBlock.Header.Number+1)
			c.logger.Infof("[%s] --> Consensus received block with vrf: %f from orderer %d for phase = %s for index = %d", nodeStateString, receivedVRFString, sender-1, req.State.String(), receivedBlockIndex)

			// Check that i can retrieve the correct last block from here

			vrfpair := vrfSenderRequest{
				vrf:              req.Vrf,
				sender:           sender,
				request:          submitReq,
				blockDataEntries: stepMsg.Entries,
				blockIndex:       receivedBlockIndex,
			}
			if len(stepMsg.Entries) == 0 {
				c.logger.Panicf("ERRRRRR I received entries = 0 for index = %v", receivedBlockIndex)
			}
			select {
			case c.vrfC <- vrfpair:
				c.logger.Infof("[%s] --> We just sent the VRF from Consensus with the block to the c.vrfC channel", nodeStateString)
			}

			//c.logger.Infof("waiting data entries confirmation for index")
			//_ = <-c.blockDataEntriesConfirmationC
			//c.logger.Infof("In Consensus received data entries confirmation for index")

			if receivedBlockProposals {
				c.logger.Infof("[%s] --> Signaling quorumProposals4Index for index = %v", nodeStateString, receivedBlockIndex)
				c.quorumProposals4IndexC <- receivedBlockIndex
			}

		}
	} else {
		// ----------------------------------------------------------------------------------------
		// c.Node.state = PreConsensusState
		c.logger.Infof("-------------------c.Consensus body-------------------------")
		if err := c.isRunning(); err != nil {
			return err
		}

		stepMsg := &raftpb.Message{}
		if err := proto.Unmarshal(req.Payload, stepMsg); err != nil {
			return fmt.Errorf("failed to unmarshal StepRequest payload to Raft Message: %s", err)
		}

		if stepMsg.To != c.raftID {
			c.logger.Warnf("Received msg to %d, my ID is probably wrong due to out of date, cowardly halting", stepMsg.To)
			c.halt()
			return nil
		}

		c.logger.Infof("Type of msg is: ", stepMsg.Type.String())
		c.logger.Infof("Follower node applying changes...")
		c.applyC <- apply{stepMsg.Entries}

		c.counter = c.counter + 1
		if c.counter == 3 && c.opts.RaftID != 3 {
			c.logger.Infof("[BLOCKPROPOSAL] --> Going into BLOCKPROPOSAL")
			c.Node.state = BlockProposal

			for raftID, consenter := range c.opts.Consenters {
				clientCert, _ := pem.Decode(consenter.ClientTlsCert)
				certPublicKeyCertificate, err := x509.ParseCertificate(clientCert.Bytes)
				if err != nil {
					c.logger.Panicf("Error in parsing certificate = %v", err)
				}
				certPubKey := certPublicKeyCertificate.PublicKey.(*ecdsa.PublicKey)
				certPubKeyHex := hex.EncodeToString(elliptic.Marshal(certPubKey, certPubKey.X, certPubKey.Y))
				c.logger.Infof("raftID = %v PublicKey = %v", raftID, certPubKeyHex)

				// initialize c.orderersPublicKeys
				c.orderersPublicKeys[raftID] = certPubKey
				c.logger.Infof("c.orderersPublicKeys[%v] = %v", raftID, certPubKeyHex)

				// print and store node's TLS private key
				filePath := "/var/hyperledger/orderer/tls/server.key"
				dataPrivateKey, err := os.ReadFile(filePath)
				if err != nil {
					c.logger.Panicf("An error occured during read of the private key file --> %v", err)
				}
				pemBlock, _ := pem.Decode(dataPrivateKey)
				priv, e := x509.ParsePKCS8PrivateKey(pemBlock.Bytes)
				privateKey := priv.(*ecdsa.PrivateKey)
				if e != nil {
					c.logger.Panicf("error in ParsePKCS8PrivateKey = %v", e)
				}
				c.logger.Infof("Our tls private key is = %v", privateKey)
				c.Node.privateKeyTLS = privateKey
			}
		}
	}

	return nil
}

func (c *Chain) configureComm() error {
	// Reset unreachable map when communication is reconfigured
	c.Node.unreachableLock.Lock()
	c.Node.unreachable = make(map[uint64]struct{})
	c.Node.unreachableLock.Unlock()

	nodes, err := c.remotePeers()
	if err != nil {
		return err
	}

	c.configurator.Configure(c.channelID, nodes)
	return nil
}

func (c *Chain) remotePeers() ([]cluster.RemoteNode, error) {
	c.raftMetadataLock.RLock()
	defer c.raftMetadataLock.RUnlock()

	var nodes []cluster.RemoteNode
	for raftID, consenter := range c.opts.Consenters {
		// No need to know yourself
		if raftID == c.raftID {
			continue
		}
		serverCertAsDER, err := pemToDER(consenter.ServerTlsCert, raftID, "server", c.logger)
		if err != nil {
			return nil, errors.WithStack(err)
		}
		clientCertAsDER, err := pemToDER(consenter.ClientTlsCert, raftID, "client", c.logger)
		if err != nil {
			return nil, errors.WithStack(err)
		}
		c.logger.Infof(fmt.Sprintf("cluster's endpoint --> %s:%d", consenter.Host, consenter.Port))
		nodes = append(nodes, cluster.RemoteNode{
			ID:            raftID,
			Endpoint:      fmt.Sprintf("%s:%d", consenter.Host, consenter.Port),
			ServerTLSCert: serverCertAsDER,
			ClientTLSCert: clientCertAsDER,
		})
	}
	return nodes, nil
}

func (c *Chain) isRunning() error {

	select {
	case <-c.startC:
	default:
		return errors.Errorf("chain is not started")
	}
	select {
	case <-c.doneC:
		return errors.Errorf("chain is stopped")
	default:
	}

	return nil
}

// Orders the envelope in the `msg` content. SubmitRequest.
// Returns
//
//	-- batches [][]*common.Envelope; the batches cut,
//	-- pending bool; if there are envelopes pending to be ordered,
//	-- err error; the error encountered, if any.
//
// It takes care of config messages as well as the revalidation of messages if the config sequence has advanced.
func (c *Chain) ordered(msg *orderer.SubmitRequest) (batches [][]*common.Envelope, pending bool, err error) {
	c.logger.Infof("chain.ordered")
	seq := c.support.Sequence()

	isconfig, err := c.isConfig(msg.Payload)
	if err != nil {
		return nil, false, errors.Errorf("bad message: %s", err)
	}

	c.logger.Info("is config = ", isconfig)

	// TODO examine if i need to uncomment it sometime
	// in our use case there will not be a config
	/*
		if isconfig {
			// ConfigMsg
			if msg.LastValidationSeq < seq {
				c.logger.Warnf("Config message was validated against %d, although current config seq has advanced (%d)", msg.LastValidationSeq, seq)
				msg.Payload, _, err = c.support.ProcessConfigMsg(msg.Payload)
				if err != nil {
					c.Metrics.ProposalFailures.Add(1)
					return nil, true, errors.Errorf("bad config message: %s", err)
				}
			}

			if c.checkForEvictionNCertRotation(msg.Payload) {

				if !atomic.CompareAndSwapUint32(&c.leadershipTransferInProgress, 0, 1) {
					c.logger.Warnf("A reconfiguration transaction is already in progress, ignoring a subsequent transaction")
					return
				}

				go func() {
					defer atomic.StoreUint32(&c.leadershipTransferInProgress, 0)

					for attempt := 1; attempt <= AbdicationMaxAttempts; attempt++ {
						if err := c.Node.abdicateLeadership(); err != nil {
							// If there is no leader, abort and do not retry.
							// Return early to prevent re-submission of the transaction
							if err == ErrNoLeader || err == ErrChainHalting {
								return
							}

							// If the error isn't any of the below, it's a programming error, so panic.
							if err != ErrNoAvailableLeaderCandidate && err != ErrTimedOutLeaderTransfer {
								c.logger.Panicf("Programming error, abdicateLeader() returned with an unexpected error: %v", err)
							}

							// Else, it's one of the errors above, so we retry.
							continue
						} else {
							// Else, abdication succeeded, so we submit the transaction (which forwards to the leader)
							if err := c.Submit(msg, 0); err != nil {
								c.logger.Warnf("Reconfiguration transaction forwarding failed with error: %v", err)
							}
							return
						}
					}

					c.logger.Warnf("Abdication failed too many times consecutively (%d), aborting retries", AbdicationMaxAttempts)
				}()
				return nil, false, nil
			}

			batch := c.support.BlockCutter().Cut()
			batches = [][]*common.Envelope{}
			if len(batch) != 0 {
				batches = append(batches, batch)
			}
			batches = append(batches, []*common.Envelope{msg.Payload})
			return batches, false, nil
		}
	*/
	// it is a normal message
	if msg.LastValidationSeq < seq {
		c.logger.Warnf("Normal message was validated against %d, although current config seq has advanced (%d)", msg.LastValidationSeq, seq)
		if _, err := c.support.ProcessNormalMsg(msg.Payload); err != nil {
			// TODO maybe add metrics....
			// c.Metrics.ProposalFailures.Add(1)
			return nil, true, errors.Errorf("bad normal message: %s", err)
		}
	}

	// TODO remove it
	nodeStateString := consensusStateToString(c.Node.state)

	c.logger.Infof("[%s] BlockCutter.Ordered Starting", nodeStateString)
	batches, pending = c.support.BlockCutter().Ordered(msg.Payload)
	c.logger.Infof("[%s] batches len after BlockCutter.Ordered = %d", nodeStateString, len(batches))
	//c.logger.Infof("[%s] msg.Payload: ", msg.Payload)
	return batches, pending, nil
}

func (c *Chain) isConfig(env *common.Envelope) (bool, error) {
	h, err := protoutil.ChannelHeader(env)
	if err != nil {
		c.logger.Errorf("failed to extract channel header from envelope")
		return false, err
	}

	return h.Type == int32(common.HeaderType_CONFIG) || h.Type == int32(common.HeaderType_ORDERER_TRANSACTION), nil
}

// func (c *Chain) createTheBlockToPropose(ch chan<- *common.Block, bc *blockCreator, batches ...[]*common.Envelope) {
func (c *Chain) createTheBlockToPropose(ch chan<- blockProposalStruct, bc *blockCreator, batches ...[]*common.Envelope) {
	// TODO remove maybe ?
	nodeStateString := consensusStateToString(c.Node.state)
	//	nodevrf := c.nodeVRF

	// added those lines lately

	if hex.EncodeToString(c.nodeVRF) == "00" {
		c.logger.Warn("Our nodeVRF is uninitialized!!!!!!")

		blockNumberToBeAdded := c.lastBlock.Header.Number + 1
		seed4VRF := c.lastBlock.Header.Seed
		c.logger.Infof("[%s]createTheBlockToPropose We will calculate a vrf for block index = %v with seed = %v provided from last block header seed", nodeStateString, blockNumberToBeAdded, seed4VRF)

		_, pi, err := ecvrf.P256Sha256Tai.Prove(c.Node.privateKeyTLS, seed4VRF)
		if err != nil {
			c.logger.Panicf("[%s]Error in creating vrf: %v", nodeStateString, err)
		}
		c.logger.Infof("[%s] calculated vrf = %v from seed = %v for blockindex = %v", nodeStateString, pi, seed4VRF, blockNumberToBeAdded)
		c.nodeVRF = pi

		// Sanity check with our publicKey
		c.logger.Infof("[%s] My raftID = %v and c.orderersPublicKeys[%v] = %v", nodeStateString, c.raftID, c.raftID, c.orderersPublicKeys[c.raftID])
		_, err = ecvrf.P256Sha256Tai.Verify(c.orderersPublicKeys[c.raftID], seed4VRF, pi)
		if err != nil {
			c.logger.Infof("Sanity check of vrf has an error = %v", err)
		} else {
			c.logger.Infof("Sanity check of vrf is ok ")
		}

		nodeVRFHex := hex.EncodeToString(c.nodeVRF)
		bestVRFHex := hex.EncodeToString(c.bestVRF.vrf)

		if nodeVRFHex > bestVRFHex {
			c.bestVRF.sender = 0
			c.bestVRF.vrf = c.nodeVRF
			//	c.raftMetadataLock.Unlock()
			c.logger.Infof("[%s] Current best vrf is calculated by us: %v", nodeStateString, c.nodeVRF)
		}

	}

	nodevrf := c.nodeVRF

	for _, batch := range batches {
		c.logger.Infof("[%s] --> we will calculate the block to propose", nodeStateString)
		// TODO our VRF needs to be added here
		//seedVRF := []byte("seedVRF")

		//b := bc.createNextBlock(batch, seedVRF)
		b := bc.createNextBlock(batch)
		c.logger.Infof("[%s] --> Created block number = %v, with vrf = %v there are %v blocks in flight", nodeStateString, b.Header.Number, nodevrf, c.blockInflight)

		blockPropStruct := blockProposalStruct{
			blockData: b,
			nodeVRF:   nodevrf,
		}

		// save vrf with blockDataEntries
		c.ownVRF = nodevrf
		blockData := protoutil.MarshalOrPanic(b)
		blockDataEntries := []pb.Entry{{Data: blockData}}
		// TODO reset it in the Delete function
		c.ownBlockDataEntries = blockDataEntries

		select {
		// This channel is read from the becomeLeader propc
		case ch <- blockPropStruct:
		default:
			c.logger.Panic("[BLOCKPROPOSAL] --> Programming error: limit of in-flight blocks does not properly take effect or block is proposed by follower")
		}

		c.blockInflight++

		// TODO this if debugging purposes might remove it later
		if c.blockInflight > 1 {
			c.logger.Warnf("[%s] More than one blocks in flight.....", nodeStateString)
		}
	}
}

func (c *Chain) propose(ch chan<- *common.Block, bc *blockCreator, batches ...[]*common.Envelope) {
	c.logger.Infof("In c.propose before entering the for loop")

	for _, batch := range batches {
		c.logger.Infof("In c.propose for loop")
		//initSeed := []byte("helloFirstSeed")
		//b := bc.createNextBlock(batch, initSeed)
		b := bc.createNextBlock(batch)
		c.logger.Infof("Created block [%d], there are %d blocks in flight", b.Header.Number, c.blockInflight)

		select {
		// This channel is read from the becomeLeader propc
		case ch <- b:
		default:
			c.logger.Panic("Programming error: limit of in-flight blocks does not properly take effect or block is proposed by follower")
		}

		// if it is config block, then we should wait for the commit of the block
		// TODO uncomment it when time comes
		//if protoutil.IsConfigBlock(b) {
		//	c.configInflight = true
		//}

		c.blockInflight++
	}
}

func (c *Chain) broadcastVRF(req *orderer.SubmitRequest) error {
	c.logger.Info("Broadcasting our newly calculated VRF to all other nodes")
	timer := time.NewTimer(c.opts.RPCTimeout)
	defer timer.Stop()

	//sentChan := make(chan struct{})
	//atomicErr := &atomic.Value{}

	/*
		report := func(err error) {
			if err != nil {
				atomicErr.Store(err.Error())
				//c.Metrics.ProposalFailures.Add(1)
			}
			close(sentChan)
		}
	*/

	eratPeers := RaftPeers(c.Node.metadata.ConsenterIds)

	for _, peer := range eratPeers {
		if peer.ID == c.Node.nodeID {
			continue // we do not want to send it to ourselves
		}
		sentChan := make(chan struct{})
		atomicErr := &atomic.Value{}

		report := func(err error) {
			if err != nil {
				atomicErr.Store(err.Error())
				//c.Metrics.ProposalFailures.Add(1)
			}
			close(sentChan)
		}

		c.logger.Infof("Before sending c.rpc.SendSubmit to %d", peer.ID)
		c.rpc.SendSubmit(peer.ID, req, report)
		c.logger.Infof("After c.rpc.SendSubmit to %d", peer.ID)
		//}

		select {
		case <-sentChan:
		case <-c.doneC:
			return errors.Errorf("chain is stopped")
		case <-timer.C:
			return errors.Errorf("timed out (%v) waiting on forwarding to %d", c.opts.RPCTimeout, peer.ID)
		}

		if atomicErr.Load() != nil {
			return errors.Errorf(atomicErr.Load().(string))
		}
	}
	return nil

}

func (c *Chain) forwardToLeader(lead uint64, req *orderer.SubmitRequest) error {
	c.logger.Infof("--------------------------")
	c.logger.Infof("Forwarding transaction to the leader %d", lead)
	timer := time.NewTimer(c.opts.RPCTimeout)
	defer timer.Stop()

	sentChan := make(chan struct{})
	atomicErr := &atomic.Value{}

	report := func(err error) {
		if err != nil {
			atomicErr.Store(err.Error())
			//c.Metrics.ProposalFailures.Add(1)
		}
		close(sentChan)
	}

	// TODO i am manipulating the ordererRequest
	// why ? ?
	if c.counter >= 4 {
		//req.Vrf = 7.8
		req.Vrf = []byte("hello")
	}

	c.logger.Infof("Before c.rpc.SendSubmit")
	c.rpc.SendSubmit(lead, req, report)
	c.logger.Infof("After c.rpc.SendSubmit")

	select {
	case <-sentChan:
	case <-c.doneC:
		return errors.Errorf("chain is stopped")
	case <-timer.C:
		return errors.Errorf("timed out (%v) waiting on forwarding to %d", c.opts.RPCTimeout, lead)
	}

	if atomicErr.Load() != nil {
		return errors.Errorf(atomicErr.Load().(string))
	}
	return nil
}

func (c *Chain) apply(ents []raftpb.Entry) {
	if len(ents) == 0 {
		c.logger.Warnf("apply was invoked with ents = 0")
		return
	}

	nodeStateString := consensusStateToString(c.Node.state)
	//var position int
	//if c.Node.state == CommitPhase {
	if nodeStateString == "Commit" {
		c.logger.Info("[%s] --> Inside apply", nodeStateString)
	}

	//nodeStateString := consensusStateToString(c.Node.state)

	// TODO check the cases as it is in raft
	for i := range ents {
		switch ents[i].Type {
		case raftpb.EntryNormal:
			if len(ents[i].Data) == 0 {
				c.logger.Warnf("length of data in entries is 0")
				break
			}
			block := protoutil.UnmarshalBlockOrPanic(ents[i].Data)
			if c.Node.state == CommitPhase {
				c.logger.Infof("[%s] --> About to write block", nodeStateString)
			}
			c.writeBlock(block, ents[i].Index)
			// TODO maybe uncomment in the future
			if c.Node.state == CommitPhase {
				c.logger.Infof("[%s] --> writeBlock finished", nodeStateString)
			}
			//c.logger.Infof("Applied block with index = %v and block header seed = %v", c.lastBlock.Header.Number, string(c.lastBlock.Header.Seed))
			//c.Metrics.CommittedBlockNumber.Set(float64(block.Header.Number))
		}
	}

	// TODO in raft here was a snapshot snippet
}

func (c *Chain) writeBlock(block *common.Block, index uint64) {

	nodeStateString := consensusStateToString(c.Node.state)
	if block.Header.Number > c.lastBlock.Header.Number+1 {
		//if c.Node.state == CommitPhase {
		if nodeStateString == "Commit" {
			c.logger.Panicf("[%s] Got block [%d], expect block [%d]", nodeStateString, block.Header.Number, c.lastBlock.Header.Number+1)
		}
		c.logger.Panicf("[%s] Got block [%d], expect block [%d]", nodeStateString, block.Header.Number, c.lastBlock.Header.Number+1)
	} else if block.Header.Number < c.lastBlock.Header.Number+1 {
		//if c.Node.state == CommitPhase {
		if nodeStateString == "Commit" {
			c.logger.Infof("[%s] --> Got block [%d], expect block [%d], this node was forced to catch up", nodeStateString, block.Header.Number, c.lastBlock.Header.Number+1)
		}
		c.logger.Infof("[%s] Got block [%d], expect block [%d], this node was forced to catch up", nodeStateString, block.Header.Number, c.lastBlock.Header.Number+1)
		return
	}

	if c.blockInflight > 0 {
		c.blockInflight-- // only reduce on leader
	}

	c.lastBlock = block
	if c.lastBlock.Header.Number == 3 {
		c.lastBlock.Header.Seed = []byte("InitSeed")
	} else {
		//c.lastBlock.Header.Seed = c.bestVRF.vrf
		c.lastBlock.Header.Seed = c.votedVRF
		c.logger.Infof("[%s] adding the votedVRF = %v index = %v", nodeStateString, c.votedVRF, c.lastBlock.Header.Number)
	}
	c.logger.Infof("[%s] Adding the bestVRF = %v to the block index = %v header as the seed that will be used for the next one", nodeStateString, c.lastBlock.Header.Seed, c.lastBlock.Header.Number)
	//c.lastBlock.Header.Seed = c.bestVRF.vrf

	c.logger.Infof("[%s] Writing block [%d] (Raft index: %d) to ledger", nodeStateString, block.Header.Number, index)
	//if c.Node.state == CommitPhase {
	if nodeStateString == "Commit" {
		c.logger.Infof("[%s] --> Writing block [%d] (Raft index: %d) to ledger", nodeStateString, block.Header.Number, index)
	}

	// TODO Currenlty no logic supported for config blocks
	// adding a node etc....
	if protoutil.IsConfigBlock(block) {
		//c.writeConfigBlock(block, index)
		c.logger.Infof("Block currently processing is a ConfigBlock")
		return
	}

	c.raftMetadataLock.Lock()
	c.opts.BlockMetadata.RaftIndex = index
	m := protoutil.MarshalOrPanic(c.opts.BlockMetadata)
	c.raftMetadataLock.Unlock()

	//if c.Node.state == CommitPhase {
	if nodeStateString == "Commit" {
		c.logger.Infof("[%s] --> before invoking c.support.WriteBlock()", nodeStateString)
	}
	c.support.WriteBlock(block, m)
}
