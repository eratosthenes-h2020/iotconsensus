package eratConsensus

import (
	"context"
	"crypto/ecdsa"
	"crypto/sha256"
	"sync"
	"sync/atomic"

	"github.com/davecgh/go-spew/spew"
	"github.com/gogo/protobuf/proto"
	"github.com/hyperledger/fabric-protos-go/orderer"
	"github.com/hyperledger/fabric-protos-go/orderer/etcdraft"
	"github.com/hyperledger/fabric/common/flogging"
	"github.com/hyperledger/fabric/protoutil"
	"go.etcd.io/etcd/raft/v3/raftpb"
	pb "go.etcd.io/etcd/raft/v3/raftpb"
)

type ConsensusState int

const (
	PreConsesusState ConsensusState = iota + 1
	BlockProposal
	VotingPhase
	CommitPhase
)

func consensusStateToString(conState ConsensusState) string {
	switch conState {
	case PreConsesusState:
		return "PreConsesusState"
	case BlockProposal:
		return "BlockProposal"
	case VotingPhase:
		return "Voting"
	case CommitPhase:
		return "Commit"
	default:
		return "Not a known ConsensusState"
	}
}

type node struct {
	chainID string
	logger  *flogging.FabricLogger
	//metrics *Metrics

	// it is used in configureComm
	unreachableLock sync.RWMutex
	unreachable     map[uint64]struct{}

	// TODO I added this do not forget
	nodeID uint64

	//tracker *Tracker

	//storage   *RaftStorage
	//config    *raft.Config
	confState atomic.Value // stores raft ConfState

	rpc RPC

	chain *Chain

	state ConsensusState

	//tickInterval time.Duration
	//clock        clock.Clock

	// TODO I can extract raftPeers from here
	metadata *etcdraft.BlockMetadata

	// private key of the node to be used
	privateKeyTLS *ecdsa.PrivateKey

	//leaderChangeSubscription atomic.Value

	//raft.Node
}

func (n *node) start(fresh, join bool) {
	raftPeers := RaftPeers(n.metadata.ConsenterIds)
	n.logger.Infof("Starting erat node: #peers: %v", len(raftPeers))

	var campaign bool
	if fresh {
		if join {
			// TODO code will never go here win our use case
			n.logger.Info("Starting raft node to join an existing channel")
			//n.Node = raft.RestartNode(n.config)
		} else {
			n.logger.Info("--------------------------")
			n.logger.Info("Starting raft node as part of a new channel")

			// determine the node to start campaign by selecting the node with ID equals to:
			//                hash(channelID) % cluster_size + 1
			sha := sha256.Sum256([]byte(n.chainID))
			number, _ := proto.DecodeVarint(sha[24:])
			if n.nodeID == number%uint64(len(raftPeers))+1 {
				campaign = true
			}
			// TODO i removed totally the raft node
			//n.Node = raft.StartNode(n.config, raftPeers)
		}
	} else {
		// TODO i have not implemented the restart service
		// TODO in our case we cannot see this message
		n.logger.Info("Restarting raft node")
		//n.Node = raft.RestartNode(n.config)
	}

	// TODO I added this code
	// I am the leader
	n.logger.Infof("------RaftPeers START-------")
	spew.Dump(raftPeers)
	n.logger.Infof("------RaftPeers END-------")
	if n.nodeID == 3 {
		n.logger.Infof("I am node with id %d and i will become the leader for our dummy example", n.nodeID)
		n.chain.leader = 1
	} else {
		n.logger.Infof("I am node with id %d and i will become a follower for our dummy example", n.nodeID)
		n.chain.leader = 0
	}

	go n.run(campaign)
}

// TODO finish the run function
// we will use the rpc.SendConsensus as the communication endpoint
// to have a first communication between every cluster member

func (n *node) run(campaign bool) {
	// TODO fill using n.send

	for {
		select {
		case <-n.chain.haltC:
			n.logger.Infof("eratConsensus node stopped")
		case <-n.chain.doneC:
			n.logger.Infof("node doneC case")
			return
		}
		//TODO add the channels for sending
	}

}

// func (n *node) ProposeVRFBlock(ctx context.Context, data []byte, vrf float32, nodeStateString string, blockNumber uint64) error {
func (n *node) ProposeVRFBlock(ctx context.Context, data []byte, vrf []byte, nodeStateString string, blockNumber uint64) error {
	broadcastMsg := pb.Message{Type: pb.MsgProp, Entries: []pb.Entry{{Data: data}}}

	var messages []raftpb.Message

	//n.chain.raftMetadataLock.Lock()

	// TODO check
	if len(data) == 0 {
		n.logger.Panicf("[%s] I am trying to send a block with zero data", nodeStateString)
	}

	if nodeStateString == "BlockProposal" {
		n.logger.Infof("[%s] In node.ProposeVRFBlock i will check if i need to store my own block", nodeStateString)
	}
	//if n.chain.bestVRF.vrf == vrf && nodeStateString == "BlockProposal" {
	/*
		if hex.EncodeToString(n.chain.bestVRF.vrf) == hex.EncodeToString(vrf) && nodeStateString == "BlockProposal" {
			n.logger.Infof("[%s] In node.ProposeVRFBlock, storing my own block with vrf = %v", nodeStateString, vrf)
			n.chain.bestVRF.blockDataEntries = broadcastMsg.Entries
			n.chain.bestVRF.sender = 42
		}
		n.chain.raftMetadataLock.Unlock()
	*/

	// add From in the pb message
	broadcastMsg.From = n.nodeID
	raftPeers := RaftPeers(n.metadata.ConsenterIds)

	for _, peer := range raftPeers {
		if peer.ID == n.nodeID {
			continue
		}
		msg := broadcastMsg
		msg.To = peer.ID

		messages = append(messages, msg)
	}

	// TODO write a new send function for our algo implementation
	return n.sendProposedBlock(messages, vrf, nodeStateString, blockNumber)

}

// TODO I need to change the raftpb.Message format because i am not using the wrapper now
// func (n *node) sendProposedBlock(msgs []raftpb.Message, vrf float32, nodeStateString string, blockNumber uint64) error {
func (n *node) sendProposedBlock(msgs []raftpb.Message, vrf []byte, nodeStateString string, blockNumber uint64) error {

	// send message using the n.rpc.SendConsensus
	n.logger.Infof("---------------------n.sendProposedBlock START------------------")
	//if n.state == BlockProposal {
	if nodeStateString == "BlockProposal" {
		n.logger.Infof("[%s]In node.sendProposedBlock i am about to send the proposed block to everyone", nodeStateString)
	}
	//if n.state == VotingPhase {
	if nodeStateString == "Voting" {
		n.logger.Infof("[%s]In node.sendProposedBlock i am about to send the proposed vote block to everyone", nodeStateString)
	}

	for _, msg := range msgs {
		if msg.To == 0 {
			n.logger.Infof("I got a msg with To = 0 continuing")
			continue
		}

		//var consReq *orderer.ConsensusRequest
		var nodePhase orderer.NodeState

		if nodeStateString == "BlockProposal" {
			n.logger.Infof("[%s]I am about to send a Consensus proposal to orderer%d with vrf = %v for block = %v", nodeStateString, msg.To-1, vrf, blockNumber)
			nodePhase = orderer.NodeState_BlockProposal
		}
		if nodeStateString == "Voting" {
			n.logger.Infof("[%s]I am about to send a Consensus vote to orderer%d with vrf = %v for block = %v", nodeStateString, msg.To-1, vrf, blockNumber)
			nodePhase = orderer.NodeState_VotingPhase
		}
		msgBytes := protoutil.MarshalOrPanic(&msg)
		// Sending the Consensus Request to everyone containing the vrf and the proposed block as well
		err := n.rpc.SendConsensus(msg.To, &orderer.ConsensusRequest{Channel: n.chainID, Payload: msgBytes, Vrf: vrf, State: nodePhase})

		if err != nil {
			n.logger.Infof("Oops there is an error sending the consensus request to node: %d", msg.To)
			n.logger.Infof("err: %s", err.Error())

		}
	}

	n.logger.Infof("---------------------n.sendProposedBlock END------------------")

	// After that we are going to apply our changes so every node needs to be synced
	// create the apply struct for the chain
	// TODO Add a for loop to support []msgs

	//n.logger.Infof("Leader node here proposed and Applying changes....")
	//n.chain.applyC <- apply{msgs[0].Entries}

	if nodeStateString == "BlockProposal" {
		n.logger.Infof("[%s] In node.sendProposedBlock i sent the proposed block to everyone succesfully invoking c.blockSentToEveryoneC", nodeStateString)
		n.chain.blockSentToEveryoneC <- struct{}{}
		return nil
	}

	if nodeStateString == "Voting" {
		n.logger.Infof("[%s] In node.sendProposedBlock i sent the a vote for the proposed block to everyone succesfully invoking c.blockVotesToEveryoneC", nodeStateString)
		go func() { n.chain.blockVotesToEveryoneC <- struct{}{} }()
	}

	return nil
}

func (n *node) Propose(ctx context.Context, data []byte) error {
	broadcastMsg := pb.Message{Type: pb.MsgProp, Entries: []pb.Entry{{Data: data}}}

	var messages []raftpb.Message

	// add From in the pb message
	broadcastMsg.From = n.nodeID
	raftPeers := RaftPeers(n.metadata.ConsenterIds)

	for _, peer := range raftPeers {
		if peer.ID == n.nodeID {
			continue
		}
		msg := broadcastMsg
		msg.To = peer.ID

		messages = append(messages, msg)
	}

	return n.send(messages)

}

// TODO I need to change the raftpb.Message format because i am not using the wrapper now
func (n *node) send(msgs []raftpb.Message) error {
	// send message using the n.rpc.SendConsensus
	n.logger.Infof("---------------------n.send START------------------")
	n.logger.Infof("In node.send i am about to send ")

	for _, msg := range msgs {
		if msg.To == 0 {
			n.logger.Infof("I got a msg with To = 0 continuing")
			continue
		}

		n.logger.Infof("I am about to send a Consensus to node %d", msg.To)
		msgBytes := protoutil.MarshalOrPanic(&msg)
		err := n.rpc.SendConsensus(msg.To, &orderer.ConsensusRequest{Channel: n.chainID, Payload: msgBytes})
		if err != nil {
			n.logger.Infof("Oops there is an error sending the consensus request to node: %d", msg.To)
			n.logger.Infof("err: %s", err.Error())

		}
	}
	n.logger.Infof("---------------------n.send END------------------")

	// After that we are going to apply our changes so every node needs to be synced
	// create the apply struct for the chain
	// TODO Add a for loop to support []msgs

	n.logger.Infof("Leader node here proposed and Applying changes....")
	n.chain.applyC <- apply{msgs[0].Entries}

	return nil
}
