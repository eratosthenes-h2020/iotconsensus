#!/bin/bash

echo $1

pathToContainerLogs=$(docker inspect --format='{{.LogPath}}' $1)
#cat $pathToContainerLogs | grep VOTINGPHASE
if [ "$2" != "consensus" ]; then
	#cat $pathToContainerLogs | grep VOTINGPHASE
	cat $pathToContainerLogs | grep Voting 
else
	#cat $pathToContainerLogs | grep VOTINGPHASE | grep "Consensus received"
	cat $pathToContainerLogs | grep Voting | grep -E "\"Consensus received\" | \"I am about to send\""
fi
