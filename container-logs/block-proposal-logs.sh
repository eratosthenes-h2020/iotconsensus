#!/bin/bash

echo $1

pathToContainerLogs=$(docker inspect --format='{{.LogPath}}' $1)

if [ "$2" != "consensus" ]; then
	#cat $pathToContainerLogs | grep BLOCKPROPOSAL
	cat $pathToContainerLogs | grep BlockProposal
else
	#cat $pathToContainerLogs | grep BLOCKPROPOSAL | grep "Consensus received"
	cat $pathToContainerLogs | grep BlockProposal | grep "Consensus received"
fi


